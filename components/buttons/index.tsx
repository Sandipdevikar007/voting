import './buttons.less';

import * as React from 'react';

interface IButtonProperties {
    uiid?: string;
    label?: string;
    variant?: string;
    onClick?: React.MouseEventHandler<HTMLButtonElement>;
    disabled?: boolean;
    active?: boolean;
    notificationsCount?: number;
    className?: string;
    buttonRef?: React.Ref<HTMLButtonElement>;
    wcaglabel?: string
    id?: string;
    type?: 'button' | 'submit' | 'reset';
}


const Button: React.StatelessComponent<IButtonProperties> = (props) => {
    return (
        <button
            aria-label={props.wcaglabel}
            ref={props.buttonRef}
            type={props.type ? props.type : 'button'}
            id={props.id}
            onClick={props.onClick}
            className={
                `wa-button ${props.className} ${props.uiid}`
                + `${props.active ? ' active' : ''}`
                + (props.className ? ` ${props.className}` : '')
            }
            disabled={props.disabled}>
            {props.label}
            {props.children}
        </button>
    )

};



export {
    Button
};
