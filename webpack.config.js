const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const VENDOR_LIBS = ["lodash","axios","immutable","react","react-dom","react-redux","react-router","react-router-dom","react-router-redux","redux","redux-devtools-extension","redux-form","redux-thunk"]
module.exports = {
  entry:{
    bundle: './applications/index.tsx',
    vendor: VENDOR_LIBS
  },
  devServer:{
  inline: true,
	port: 8080,
	open: true,
	historyApiFallback: true,
	disableHostCheck: true,
	headers: {
		'Access-Control-Allow-Origin': '*',
	},
	proxy: {
		'/api': {
			target: "http://myonlinebook.herokuapp.com",
			secure: false,
			changeOrigin: true,
		}
	},
  },
  
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].[chunkhash].js'
  },
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        loader: 'awesome-typescript-loader'
      },
      {
        test: /\.(less|css)$/,
	use: [{
		loader: 'style-loader',
	}, {
		loader: 'css-loader',
	}, {
		loader: 'less-loader',
	}],
      },
      { test: /\.(js)$/, use: 'babel-loader' },
      { test: /\.css$/, use: ["style-loader", "css-loader"] },
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: ["awesome-typescript-loader"],
    },
    {
      test: /\.(png|jpe?g|gif)$/i,
      use: [
        {
          loader: 'file-loader',
        },
      ],
    }
 
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './applications/index.html'
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_NODE)
    })
  
  ],
  optimization: {
    splitChunks: {
      chunks (chunk) {
        return chunk.name !== ['vendor','manifest'];
      }
    }
  }
}