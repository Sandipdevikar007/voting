const mongoose=require("mongoose");
const config=require("config");
const db=config.get("mongoURI");

const connectDB = async () =>{
    console.log('hello');
    try{
        console.log('er2r');
        await mongoose.connect(db,{
            useUnifiedTopology:true,
                useNewUrlParser:true,
                useCreateIndex:true
                 
               
        })
        console.log("mongodb connected");
    }
    catch(err){
        console.log('err');
            console.error(err.message);
            process.exit(1);
    }
}

module.exports=connectDB;