const express=require('express');
const path= require('path');
const app = express();
app.get("/hello",(req,res)=>{
    res.send("api runns");
});
var bodyParser = require('body-parser');
const connectDB=require('./config/db');


 connectDB();
 app.use(bodyParser.json({limit: '50mb'}));
 app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
 app.use(express.json());
 app.use("../public",express.static('public'))
app.use('../images',express.static('images'));
app.use('images/',express.static('images'));
app.use('images',express.static('images'));
app.use('/images',express.static('images'));

app.use(express.static('images'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'images')));
 app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, x-auth-token"
    );
    next();
  });
app.use("/api/auth",require("./services/routes/api/auth"));
app.use("/api/product",require("./services/routes/api/product"));
app.use("/api/item",require("./services/routes/api/item"));
app.use("/api/checkout",require("./services/routes/api/checkout"));
app.use("/api/orders",require("./services/routes/api/orders"));
app.use("/api/image",require("./services/routes/api/file-upload"));
if(process.env.NODE_ENV !== 'production'){

    const webpackMiddleware = require('webpack-dev-middleware');
    const webpack = require('webpack');
    const webpackConfig = require('./webpack.config.js')
    app.use(webpackMiddleware(webpack(webpackConfig)));
}else{
    app.use(express.static('dist'));
    app.get('*',(req,res)=>{
        res.sendFile(path.resolve(__dirname,'dist/index.html'));
    });
}
const PORT=process.env.PORT||5000;

app.listen(PORT,()=>{
    console.log("api running");
}); 