
const ProductList = require("../../models/product");
const express = require("express");
const router = express.Router();
const User = require("../../models/user");
const { check, validationResult } = require("express-validator/check");
const crypto = require('crypto');
const bcrypt = require("bcryptjs");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");
const config=require("config");
const Order = require("../../models/orders");
const auth = require("../../middleware/auth");
router.get("/", auth,async (req, res) => {
    try {
        const order = await Order.find();
        res.json(order)
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
module.exports = router;