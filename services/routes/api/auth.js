const express = require("express");
const router = express.Router();
const User = require("../../models/user");
const { check, validationResult } = require("express-validator/check");
const crypto = require('crypto');
const bcrypt = require("bcryptjs");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");
const auth = require("../../middleware/auth");
const config=require("config");
const {sendWelcomeMail} = require("../../mail/mailer");
router.get("/",auth,async (req,res)=>{

    try {
       const user= await User.findById(req.user.id).select("-password");
        res.json(user)
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({msg:"server error"});
    
    }    
    })
    
router.post("/", [
    check("name", "Name is require").not().isEmpty(),
    check("email", "Email is invalid").isEmail(),
    check("password", "password min 6 length").isLength({ min: 6 })

], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ msg: errors.array() });
    }
    const {
        name,
        email,
        password
    } = req.body;

    try {
        let user = await User.findOne({ email });
        if (user) {
            return res
                .status(400)
                .json({ errors: [{ msg: 'User already exists' }] });
        }
        const avatar = gravatar.url(email, {
            s: '200',
            r: 'pg',
            d: 'mm'
        });

        user = new User({
            name, email, avatar, password, totalPrice: 0
        });
        const salt = await bcrypt.genSalt(10);

        user.password = await bcrypt.hash(password, salt);

        await user.save();
        const payload = {
            user: {
                id: user.id
            }
        };

        jwt.sign(
            payload,
            config.get('jwtSecret'),
            { expiresIn: 360000 },
            (err, token) => {
                if (err) throw err;
                res.json({ token });
                return sendWelcomeMail(email, name);


            }
        );
    } catch (err) {
        console.error(err);
        return res.status(500).send('Server error');
    }

});
router.post("/login",[

    check("email","Invalid email").isEmail(),
    check("password","Invalid Password").exists()





], async (req,res)=>{
    console.log('login')
const errors=validationResult(req);
    if(!errors.isEmpty()){

        return res.status(400).json({msg:errors.array()});

    }
    try{
            const { email,password} =req.body;

            let user = await User.findOne({email});
            if(!user){

                return res.status(400).json({ errors: [{ msg: 'User not exists' }] });
            }
         const v=await bcrypt.compare(password,user.password);
         if(!v){
                return res.status(400).json({ errors: [{ msg: 'Invalid password' }] });
         }

         const payload = {
            user: {
              id: user.id
            }
          };
    
          jwt.sign(
            payload,
            config.get('jwtSecret'),
            { expiresIn: 360000 },
            (err, token) => {
              if (err) throw err;
              res.json({ token });
            }
          );
    }catch(err){

    }
});

module.exports = router;