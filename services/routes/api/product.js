
const ProductList = require("../../models/product");
const express = require("express");
const router = express.Router();
const multer = require('multer');
const User = require("../../models/user");
const { check, validationResult } = require("express-validator/check");
const crypto = require('crypto');
const bcrypt = require("bcryptjs");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");
const config=require("config");
const algoliasearch = require('algoliasearch');

const client = algoliasearch('L7BZWYQ8XL', '7d2550fe12d13f8607e3e98a848178f4');
const index = client.initIndex('test_PRODUCTS');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'images/')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    }
  
  });
  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 50000000000
    },
  
  })
router.get("/", async (req, res) => {
    try {
        // Search for a first name
        index.search('').then(({ hits }) => {
            res.json(hits);
        });
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})


// const product = await ProductList.find({
//     "$text": {
//         "$search": req.query.param
//     }
// });
router.post("/search", async (req, res) => {
    let q = req.query.param;
    try {
        let query = {
            "$or": [{"name": {"$regex": q, "$options": "i"}}, {"author": {"$regex": q, "$options": "i"}},
            {"categories": {"$regex": q, "$options": "i"}}]
          };
        const product = await ProductList.find(query);
        res.json(product)
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
router.post('/search/suggestion', async (req, res) => {
    try{
        index.search(req.body.query,{
            filters: '(author:Dr. Joseph Murphy)'
          }).then(({ hits }) => {
            res.json(hits);
        });
    }catch(err){
        res.status(400).json(err)
    }
  
  });
router.post('/', async (req, res) => {
    const {
        name,
        sku,
        productId,
        imageUrl,
        author,
        platform,
        additionalDescription,
        description,
        additionalImages,
        features,
        rating,
        discount,
        fromPrice,
        categories
    } = req.body;
    try {
        let product = await ProductList.findOne({ productId });
        if (product) {
            return res
                .status(400)
                .json({ errors: [{ msg: 'Product already exists' }] });
        }
        product = new ProductList({
            name,
            sku,
            productId,
            imageUrl,
            author,
            platform,
            additionalDescription,
            description,
            additionalImages,
            features,
            rating,
            fromPrice,
            discount,
            categories
        })
        await index.saveObject(product,{
            autoGenerateObjectIDIfNotExist: true
        }).then((obj)=> console.log(obj)).catch(err=> console.log(err))
        await product.save();
        res.json(product)
    } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: err });
    }

});


router.post('/update', async (req, res) => {
    const {
        name,
        sku,
        productId,
        imageUrl,
        author,
        platform,
        additionalDescription,
        description,
        additionalImages,
        features,
        rating,
        discount,
        fromPrice,
        categories
    } = req.body;
    try {
        let product = await ProductList.updateOne({ productId },{
            name,
            sku,
            imageUrl,
            author,
            platform,
            additionalDescription,
            description,
            additionalImages,
            features,
            rating,
            discount,
            fromPrice,
            categories
        }, { upsert: true });
        console.log(product)
        res.json({
            data: "update success"
        })
    } catch (err) {
        console.log(err);
        return res.status(500).json({ msg: err });
    }

});










router.get("/detail/:id", async (req, res) => {
    try {
        const product = await ProductList.find({productId: req.params.id});
        res.json(product[0])
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
module.exports = router;