const ProductList = require("../../models/product");
const express = require("express");
const router = express.Router();
const User = require("../../models/user");
const Order = require("../../models/orders");
const { check, validationResult } = require("express-validator/check");
const crypto = require('crypto');
const bcrypt = require("bcryptjs");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");
const config=require("config");
const auth = require("../../middleware/auth");
const {sendOrderPlacedMail} = require("../../mail/mailer");
router.get("/customer-detail", auth,async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select("-password");
        res.json(user.address)
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
router.post("/submit-order", auth,async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select("-password");
        let order= null;
        if(user.cartItems.length > 0){
             order = new Order({
                customerName: user.name,
                deliveryAddress: req.body.deliveryAddress,
                orderedItems: user.cartItems,
                totalPrice: user.totalPrice
            })
        }else{
            return res.status(401).json({ msg: "No items in cart" });
        }
        await order.save();
        const cartItems = user.cartItems;
        sendOrderPlacedMail(user,user.totalPrice, order, cartItems.length);
        user.cartItems.splice(0, user.cartItems.length);
        user.totalPrice =0;
        await user.save();
        res.json({
            message: 'submitted successfully'
        })
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
module.exports = router;