
const ProductList = require("../../models/product");
const express = require("express");
const router = express.Router();
const User = require("../../models/user");
const { check, validationResult } = require("express-validator/check");
const crypto = require('crypto');
const bcrypt = require("bcryptjs");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");
const config=require("config");
const auth = require("../../middleware/auth");
router.post("/add", auth,async (req, res) => {
    try {
        let user = await User.findById(req.user.id).select("-password");
        let {itemName, quantity, unitPrice, id, imageUrl} = req.body.product
        const index = await user.cartItems.findIndex((item) => {
            if (item.id) {
                return item.id.toString() === req.body.product.id.toString()
            }
        })
        if (index === -1) {
            await user.cartItems.unshift({
                itemName, quantity, unitPrice, id,imageUrl
            })
        } else {
           const qty = user.cartItems[index].quantity
           await user.cartItems.splice(index, 1);
           await user.cartItems.unshift({itemName, quantity: qty+quantity, unitPrice, id, imageUrl});
        }
        const totalPrice= user.totalPrice + (quantity * unitPrice)
        user.totalPrice = totalPrice;
        await user.save();
        res.json(user)
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
router.get("/saved-item", auth,async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select("-password");
        res.json(user.cartItems)
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
router.post("/update-item",auth,async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select("-password");
        const index = await user.cartItems.findIndex((item) => {
            if (item.id) {
                return item.id.toString() === req.body.product.id.toString()
            }
        })
        if (index === -1) {
            return res.status(400).json({ msg: "not found product" });
        } else {
           await user.cartItems.splice(index, 1);
           await user.cartItems.unshift(req.body.product);
        }
        await user.save();
        res.json(user.cartItems)
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
router.post("/remove-item",auth,async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select("-password");
        if(user){
            const index = await user.cartItems.findIndex((item) => {
                if (item.id) {
                    return item.id.toString() === req.body.product.id.toString()
                }
            })
            if (index === -1) {
                return res.status(400).json({ msg: "not found product" });
            } else {
                user.totalPrice = user.totalPrice - user.cartItems[index].quantity * user.cartItems[index].unitPrice;
               await user.cartItems.splice(index, 1);
            }
            await user.save();
            res.json(user.cartItems)
        }else{
            return res.status(403).json({ msg: "No user found" });
        }
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
router.get("/clear-order",auth,async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select("-password");
        user.cartItems.splice(0, user.cartItems.length);
        await user.save();
        res.json(user.cartItems)
    } catch (err) {
        console.log(err.message);
        return res.status(500).json({ msg: "server error" });
    }
})
module.exports = router;
