const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ProductsScheme = new mongoose.Schema({
    name: {
        type: String,
    },
    productId: {
        type: String
    },
    sku: {
        type: String
    },
    Date: {
        type: Date,
        default: Date.now
    },
    author: {
        type: String
    },
    platform: {
        type: String
    },
    imageUrl: {
        type: String
    },
    fromPrice: {
        type: String
    },
    additionalDescription: {
        type: String
    },
    description: {
        type: String
    },
    additionalImages: [{
        type: String
    }],
    features: {
        type: String
    },
    rating: {
        type: String
    },
    discount:{
        type: Number
    },
    categories:[{
        type: String
    }],
});
ProductsScheme.index({author: 'text' });
module.exports = ProductList = mongoose.model('productlist', ProductsScheme)

