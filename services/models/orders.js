const mongoose = require("mongoose");
const { numberParser } = require("config/parser");
const Schema = mongoose.Schema;
const OrdersScheme = new mongoose.Schema({
    customerName:{
        type: String
    },
    deliveryAddress: 
        {
            addressLine1: String,
            addressLine2: String,
            city: String,
            state: String,
            pincode: Number,
            mobileNumber: Number,
            country: {
                type:String,
                default:"India"
        }
    },
    orderedItems:[
        {
            id: String,
            itemName:{
                type:String
            },
            quantity:{
                type: Number
            },
            unitPrice:{
                type: Number
            },
            imageUrl:{
                type: String
            }
        }
    ],
    totalPrice:{
        type: Number
    }
});
OrdersScheme.index({name: 'text'});

module.exports = Orders = mongoose.model('orders', OrdersScheme)

