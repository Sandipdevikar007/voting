const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const UserScheme = new mongoose.Schema({
        name:{
            type:String,
            required:true
        },
        email:{
            type:String,
            unique:true
        },
        password:{
            type:String,
            required:true
        },
        avatar:{
            type:String
        },
        resetTokenExpiration:Date,
        resetToken:String,
        Date:{
            type:Date,
            default:Date.now
        },
        UserType:{
            type:String,
            default:"customer"
        },
        cartItems:[
            {
                id: String,
                itemName:{
                    type:String
                },
                quantity:{
                    type: Number
                },
                unitPrice:{
                    type: Number
                },
                imageUrl:{
                    type: String
                }
            }
        ],
        totalPrice:{
            type: Number
        },
        address:[
            {
                addressLine1: String,
                addressLine2: String,
                city: String,
                state: String,
                pincode: Number,
                mobileNumber: Number,
                country: {
                    type:String,
                    default:"India"
                }

            }
        ]

});
module.exports= User =mongoose.model('user',UserScheme)

