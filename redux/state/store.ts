import { Middleware, Store, combineReducers, applyMiddleware, createStore } from "redux";
import thunk from 'redux-thunk';
import {reducer as formReducer} from 'redux-form';
import { composeWithDevTools } from 'redux-devtools-extension';

export function initStore<TStore>(reducers, middleware: Middleware[]): Store<TStore> {
    const allMiddleware = [...middleware, thunk];
    const allReducers = combineReducers<TStore>({
        ...reducers,
        form: formReducer,
    });

    return createStore(allReducers, composeWithDevTools(applyMiddleware(...allMiddleware)))
}


