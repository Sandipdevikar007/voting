import './scroll-behaviour.less';

import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';

type IScrollBehaviourProps = RouteComponentProps<{}>;

class ScrollBehaviourComponent extends React.Component<IScrollBehaviourProps, {}> {
    public static displayName = 'ScrollBehaviour';

    public render() {
        return <div>{this.props.children}</div>;
    }

    public componentDidUpdate(oldProps: IScrollBehaviourProps) {
        if (this.props.location.pathname !== oldProps.location.pathname) {
            window.scrollTo(0, 0);
        }
    }
}

const mapStateToProps = (state, ownProps) => ({
    location: ownProps.location
});

const ScrollBehaviour = withRouter(connect(mapStateToProps)(ScrollBehaviourComponent));
export { ScrollBehaviour };
