export default class OnWindowScroll {
    public getScrollOffset() {
        if (document.documentElement) {
            return {
                top:
                    'scrollY'
                     in window
                        ? window.scrollY
                        : document.documentElement.scrollTop,
                left:
                    'scrollX' in window
                        ? window.scrollX
                        : document.documentElement.scrollLeft
            };
        }
    }
    public registerListener(callback: EventListener) {
        this.unregisterListener(callback);
        window.addEventListener('scroll', callback);
    }
    public unregisterListener(callback: EventListener) {
        window.removeEventListener('scroll', callback);
    }
}

// export as single instance
export const onwindowscroll = new OnWindowScroll();
