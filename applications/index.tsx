import React from 'react';
import { render } from 'react-dom';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LandinPage from './dashboard/pages/landing-page';
import OrderSummary from './dashboard/pages/order-summary';
import Checkout from './dashboard/pages/checkout';
import OrderSuccess from './dashboard/pages/order-success'
import './index.less'
import { Provider } from 'react-redux';
import { store } from './store'
import Navbar from './dashboard/pages/Navbar';
import EmptyCart from './dashboard/pages/empty-cart';
import { ScrollBehaviour } from './utils/scroll-behaviour';
import  Orders  from './dashboard/pages/orders';
import ProductUpload from './dashboard/components/product-upload';
import ProductUpdate from './dashboard/components/product-update';
class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <ScrollBehaviour>
          <Navbar />
          <div className="main">
            <Switch>
            <Route exact path="/" component={LandinPage} />
            <Route exact path="/order-summary" component={OrderSummary} />
            <Route exact path="/checkout" component={Checkout} />
            <Route exact path="/ordersuccess" component={OrderSuccess} />
            <Route exact path="/emptycart" component={EmptyCart} />
            <Route exact path="/delivery-order" component={Orders} />
            <Route exact path="/product-upload" component={ProductUpload} />
            <Route exact path="/product-update" component={ProductUpdate} />
            <Route component={Page404}/>
            </Switch>
          </div>
        </ScrollBehaviour>
      </Provider>
    );
  }
}

const Page404 = ({ location }) => (
  <div className={'text-center flex-center'}>
     <h2>No match found for <code>{location.pathname}</code></h2>
  </div>
);
render(<Router><App /></Router>, document.getElementById('app'));
