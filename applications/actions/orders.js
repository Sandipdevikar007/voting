import axios from 'axios';
import { setAlert } from './alert';
import {
   LOAD_ORDER_INIT,
   LOAD_ORDER,
   LOAD_ORDER_ERROR,
  } from '../actions/types';
  
import setAuthToken from '../utils/setAuthToken'
// Load User
export const loadOrders =() => async dispatch => {
    dispatch({
        type:LOAD_ORDER_INIT,
        payload: "INITIATED"
      });
    try{
      const res=await axios.get(`/api/orders`);
      dispatch({
        type:LOAD_ORDER,
        payload: res.data
      });
    }catch(err){
        dispatch({
            type:LOAD_ORDER_ERROR,
            payload: err
          });
    }
  }



