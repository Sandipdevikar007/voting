import axios from 'axios';
import { setAlert } from './alert';
import {
   REMOVE_ITEM_INIT,
   REMOVE_ITEM,
   REMOVE_ITEM_ERROR,
  } from '../actions/types';
  
import setAuthToken from '../utils/setAuthToken'
// Load User
export const removeItem =(body) => async dispatch => {
    dispatch({
        type:REMOVE_ITEM_INIT,
        payload: "INITIATED"
      });
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };
      console.log(body+"sasas");
    try{
      const res=await axios.post(`api/item/remove-item`, body, config);
      dispatch({
        type:REMOVE_ITEM,
        payload: res.data
      });
    }catch(err){
        dispatch({
            type:REMOVE_ITEM_ERROR,
            payload: err
          });
    }
  }



