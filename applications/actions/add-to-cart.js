import axios from 'axios';
import { setAlert } from './alert';
import {
    ADD_TO_CART_INIT,
    ADD_TO_CART,
    LOAD_CART_INIT,
    LOAD_CART,
    LOAD_CART_ERROR,
    ADD_TO_CART_ERROR,
    SUBMIT_ORDER_INIT,
    SUBMIT_ORDER,
    SUBMIT_ORDER_ERROR
  } from '../actions/types';
  
import setAuthToken from '../utils/setAuthToken'
// Load User
export const submitCartItem =(body) => async dispatch => {
    dispatch({
        type: ADD_TO_CART_INIT,
        payload: "INITIATED"
      });
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };
    try{
      const res=await axios.post(`/api/item/add`, body, config);
      dispatch({
        type: ADD_TO_CART,
        payload: res.data
      });
    }catch(err){
        dispatch({
            type: ADD_TO_CART_ERROR,
            payload: err
          });
  
    }
  }



  export const loadCart =() => async dispatch => {
    
    dispatch({
        type: LOAD_CART_INIT,
        payload: "INITIATED"
      });
    try{
      const res= await axios.get(`/api/item/saved-item`);
      dispatch({
        type: LOAD_CART,
        payload: res.data
      });
    }catch(err){
        console.log(err)
        dispatch({
            type: LOAD_CART_ERROR,
            payload: err
          });
  
    }
  }

  export const submitOrder =(body) => async dispatch => {

    console.log('yee')
    console.log(body)
    dispatch({
        type: SUBMIT_ORDER_INIT,
        payload: "INITIATED"
      });
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };
    try{
      const res=await axios.post(`/api/checkout/submit-order`, body, config);
      dispatch({
        type: SUBMIT_ORDER,
        payload: res.data
      });
    }catch(err){
        console.log(err)
        dispatch({
            type: SUBMIT_ORDER_ERROR,
            payload: err
          });

    }
  }