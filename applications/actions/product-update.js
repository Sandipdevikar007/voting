import axios from 'axios';
import { setAlert } from './alert';
import {
    PRODUCT_UPDATE_INIT,
    PRODUCT_UPDATE,
    PRODUCT_UPDATE_ERROR,
  } from '../actions/types';
  
import setAuthToken from '../utils/setAuthToken'
// Load User
export const updateProductItem =(body) => async dispatch => {
    dispatch({
        type: PRODUCT_UPDATE_INIT,
        payload: "INITIATED"
      });
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };
    try{
        console.log(body);
      const res=await axios.post(`/api/product/update`, body, config);
      dispatch({
        type: PRODUCT_UPDATE,
        payload: res.data
      });
    }catch(err){
        dispatch({
            type: PRODUCT_UPDATE_ERROR,
            payload: err
          });
  
    }
  }

