import axios from 'axios';
import { setAlert } from './alert';
import {
  PRODUCT_LOAD,
  PRODUCT_ERROR,
  PRODUCT_DETAIL_LOAD,
  PRODUCT_LOAD_INIT,
  PRODUCT_DETAIL_LOAD_INIT,
  PRODUCT_SEARCH,
  PRODUCT_SEARCH_CANCEL,
  SEARCH_SUGGESTION,
  SEARCH_SUGGESTION_INIT
} from './types';
import setAuthToken from '../utils/setAuthToken';

const algoliasearch = require('algoliasearch');
const client = algoliasearch('L7BZWYQ8XL', '9b167ea39f08ea31f83e70159291398c');
const index = client.initIndex('test_PRODUCTS');
// Load Products
export const loadProducts =(page) => async dispatch => {
    dispatch({
        type:PRODUCT_LOAD_INIT,
        payload: "INITIATED"
      });
      dispatch({
        type:PRODUCT_SEARCH_CANCEL
      });
      
  try {
    index.search("",{
      hitsPerPage: 1000
    }).then(({ hits,nbHits }) => {

      dispatch({
        type: PRODUCT_LOAD,
        payload: {hits,nbHits}
      });

    })

    }catch(err){
      dispatch({
        type:PRODUCT_ERROR
      });
  
    }
  }
  export const searchProducts =(body) => async dispatch => {
    dispatch({
        type:PRODUCT_LOAD_INIT,
        payload: "INITIATED"
      });
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };
    try{
      index.search(body.param).then(({ hits, nbHits }) => {

        dispatch({
          type: PRODUCT_SEARCH,
          payload: { body,hits,nbHits}
        });
  
      })
    }catch(err){
      dispatch({
        type:PRODUCT_ERROR
      });
  
    }
  }
  export const searchSuggestions =(body) => async dispatch => {
    dispatch({
      type:SEARCH_SUGGESTION_INIT
    });
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };
    try{
      index.search(body.query).then(({ hits }) => {

        dispatch({
          type: SEARCH_SUGGESTION,
          payload: hits
        });
  
      })
      
    }catch(err){
    console.log(err)
  
    }
  }
  export const loadProductDetail =(productId) => async dispatch => {
      
    dispatch({
        type: PRODUCT_DETAIL_LOAD_INIT,
        payload: "INITIATED"
      });
    try{
     index.getObject(productId).then(hit=>{
      dispatch({
        type: PRODUCT_DETAIL_LOAD,
        payload: hit
      });
     })
      
    }catch(err){
      dispatch({
        type:PRODUCT_ERROR
      });
  
    }
  }

  