import axios from 'axios';
import { setAlert } from './alert';
import {
    PRODUCT_UPLOAD_INIT,
    PRODUCT_UPLOAD,
    PRODUCT_UPLOAD_ERROR,
  } from '../actions/types';
  
import setAuthToken from '../utils/setAuthToken'
// Load User
export const submitProductItem =(body) => async dispatch => {
    dispatch({
        type: PRODUCT_UPLOAD_INIT,
        payload: "INITIATED"
      });
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };
    try{
        console.log(body);
      const res=await axios.post(`/api/product`, body, config);
      dispatch({
        type: PRODUCT_UPLOAD,
        payload: res.data
      });
    }catch(err){
        dispatch({
            type: PRODUCT_UPLOAD_ERROR,
            payload: err
          });
  
    }
  }

