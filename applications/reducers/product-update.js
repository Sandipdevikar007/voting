import {
    PRODUCT_UPDATE_INIT,
    PRODUCT_UPDATE,
    PRODUCT_UPDATE_ERROR
  } from '../actions/types';
  
  const initialState = {
    productUpdateData: {
        data: [],
        status: "NOT STARTED"
    }
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case PRODUCT_UPDATE_INIT:return{
        ...state,
        productUpdateData:{
            ...state.productUpdateData,
            status: "INITIATED"
        }
      }
      case PRODUCT_UPDATE:return{
        ...state,
        productUpdateData:{
            ...state.productUpdateData,
            data: payload,
            status: "SUCCESS"
        }
      }
      case PRODUCT_UPDATE_ERROR:return{
        ...state,
        productUpdateData:{
            ...state.productUpdateData,
            status: "FAILURE"
        }
      }
      default:
        return state;
    }
  }
  