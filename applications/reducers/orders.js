import {
    LOAD_ORDER_INIT,
    LOAD_ORDER,
    LOAD_ORDER_ERROR
  } from '../actions/types';
  
  const initialState = {
    order: {
        data: [],
        status: "NOT STARTED"
    }
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case LOAD_ORDER_INIT:return{
        ...state,
        order:{
            ...state.order,
            status: "INITIATED"
        }
      }
      case LOAD_ORDER:return{
        ...state,
        order:{
            ...state.order,
            data: payload,
            status: "SUCCESS"
        }
      }
      case LOAD_ORDER_ERROR:return{
        ...state,
        order:{
            ...state.order,
            status: "FAILURE"
        }
      }
      default:
        return state;
    }
  }
  