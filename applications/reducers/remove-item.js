import {
    REMOVE_ITEM_INIT,
    REMOVE_ITEM,
    REMOVE_ITEM_ERROR
  } from '../actions/types';
  
  const initialState = {
    removeData: {
        data: [],
        status: "NOT STARTED"
    }
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case REMOVE_ITEM_INIT:return{
        ...state,
        removeData:{
            ...state.removeData,
            status: "INITIATED"
        }
      }
      case REMOVE_ITEM:return{
        ...state,
        removeData:{
            ...state.removeData,
            data: payload,
            status: "SUCCESS"
        }
      }
      case REMOVE_ITEM_ERROR:return{
        ...state,
        removeData:{
            ...state.removeData,
            status: "FAILURE"
        }
      }
      default:
        return state;
    }
  }
  