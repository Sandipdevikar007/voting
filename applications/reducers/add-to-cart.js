import {
    ADD_TO_CART_INIT,
    ADD_TO_CART,
    ADD_TO_CART_ERROR
  } from '../actions/types';
  
  const initialState = {
    submitCartItem: {
        data: [],
        status: "NOT STARTED"
    }
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case ADD_TO_CART_INIT:return{
        ...state,
        submitCartItem:{
            ...state.submitCartItem,
            status: "INITIATED"
        }
      }
      case ADD_TO_CART:return{
        ...state,
        submitCartItem:{
            ...state.submitCartItem,
            data: payload,
            status: "SUCCESS"
        }
      }
      case ADD_TO_CART_ERROR:return{
        ...state,
        submitCartItem:{
            ...state.submitCartItem,
            status: "FAILURE"
        }
      }
      default:
        return state;
    }
  }
  