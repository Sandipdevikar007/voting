import {
    PRODUCT_UPLOAD_INIT,
    PRODUCT_UPLOAD,
    PRODUCT_UPLOAD_ERROR
  } from '../actions/types';
  
  const initialState = {
    productUploadData: {
        data: [],
        status: "NOT STARTED"
    }
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case PRODUCT_UPLOAD_INIT:return{
        ...state,
        productUploadData:{
            ...state.productUploadData,
            status: "INITIATED"
        }
      }
      case PRODUCT_UPLOAD:return{
        ...state,
        productUploadData:{
            ...state.productUploadData,
            data: payload,
            status: "SUCCESS"
        }
      }
      case PRODUCT_UPLOAD_ERROR:return{
        ...state,
        productUploadData:{
            ...state.productUploadData,
            status: "FAILURE"
        }
      }
      default:
        return state;
    }
  }
  