import {
    SUBMIT_ORDER_INIT,
    SUBMIT_ORDER,
    SUBMIT_ORDER_ERROR
  } from '../actions/types';
  
  const initialState = {
    submitOrder: {
        data: [],
        status: "NOT STARTED"
    }
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case SUBMIT_ORDER_INIT:return{
        ...state,
        submitOrder:{
            ...state.submitOrder,
            status: "INITIATED"
        }
      }
      case SUBMIT_ORDER:return{
        ...state,
        submitOrder:{
            ...state.submitOrder,
            data: payload,
            status: "SUCCESS"
        }
      }
      case SUBMIT_ORDER_ERROR:return{
        ...state,
        submitOrder:{
            ...state.submitOrder,
            status: "FAILURE"
        }
      }
      default:
        return state;
    }
  }
  