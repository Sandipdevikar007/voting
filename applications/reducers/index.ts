import {combineReducers} from 'redux'
import auth  from './auth';
import alert from './alert'
import products from './productlist';
import cart from './add-to-cart';
import bucket from './cart';
import order from './submit-order';
import productOrders from './orders';
import removeItemData from './remove-item';
import productUpload from './product-upload';
import productUpdate from './product-update';
export const reducers = combineReducers({
    auth, alert, products,
    cart, bucket,
    order,
    removeItemData,
    productOrders,
    productUpload,
    productUpdate
})