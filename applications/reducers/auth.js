import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
   USER_LOADED,
   AUTH_ERROR,
   LOGIN_SUCCESS,
   LOGIN_FAIL,
   LOGOUT,
   LOGIN_INIT
  } from '../actions/types';
  import setAuthToken from '../utils/setAuthToken'
  const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    loading: true,
    user: null,
    status: "NOT STARTED"
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case USER_LOADED:return{
        ...state,
        isAuthenticated:true,
        loading:false,
        user:payload,
        status:'SUCCESS'
      }
      case LOGIN_INIT:return{
        ...state,
        status: "INITIATED"
      }
      case REGISTER_SUCCESS:
      case LOGIN_SUCCESS:
        localStorage.setItem('token', payload.token);
        return {
          ...state,
          ...payload,
          isAuthenticated: true,
          loading: false,
          status: "SUCCESS"
        };
      case REGISTER_FAIL:
        case LOGIN_FAIL:
        case AUTH_ERROR:
          case LOGOUT:
        localStorage.removeItem('token');
        setAuthToken(localStorage.token)
        return {
          ...state,
          token: null,
          isAuthenticated: false,
          user:null,
          loading: false,
          status:"FAILURE"
        };
      default:
        return state;
    }
  }
  