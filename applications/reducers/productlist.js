import {
    PRODUCT_LOAD,
    PRODUCT_ERROR,
    PRODUCT_LOAD_INIT,
    PRODUCT_DETAIL_LOAD,
    PRODUCT_DETAIL_LOAD_INIT,
    PRODUCT_SEARCH,
    PRODUCT_SEARCH_CANCEL,
    SEARCH_SUGGESTION,
    SEARCH_SUGGESTION_INIT
  } from '../actions/types';
  
  const initialState = {
    productList: {
        data:[],
        status: 'NOT STARTED',
        totalRecords:0
    },
    productDetail: {
        data: {},
        status: 'NOT STARTED'
    },
    search:{
        searchText: '',
        searchFlag: false,
        suggestions: [],
        suggestionStatus: "NOT STARTED"
    }
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
        case PRODUCT_LOAD_INIT:return{
            ...state,
            productList: {
                ...state.productList,
                status: payload
            }
          }
      case PRODUCT_LOAD:return{
        ...state,
        productList: {
            ...state.productList,
            data: state.productList.data.concat(payload.hits),
            totalRecords: payload.nbHits,
            status: "SUCCESS"
        }
      }
      case PRODUCT_DETAIL_LOAD:return{
        ...state,
        productDetail: {
            ...state.productDetail,
            data: payload,
            status: "SUCCESS"
        }
      }
      case PRODUCT_DETAIL_LOAD_INIT:return{
        ...state,
        productDetail: {
            ...state.productDetail,
            status: "INITIATED"
        }
      }
      case PRODUCT_LOAD_INIT:return{
        ...state,
        productList: {
            ...state.productList,
            status: payload
        }
      }
      case PRODUCT_SEARCH:return{
        ...state,
        search: {
          ...state.search,
          searchText: payload.body.param,
          searchFlag: true
        },
        productList: {
          ...state.productList,
          data: payload.hits,
          totalRecords: payload.nbHits,
          status: "SUCCESS"
      }
        
      }
      case PRODUCT_SEARCH_CANCEL:return{
        ...state,
        search: {
          ...state.search,
          searchText: '',
          searchFlag: false
        }
      }
      
      case SEARCH_SUGGESTION_INIT:return{
        ...state,
        search: {
          ...state.search,
          suggestionStatus: "INITIATED"
        }
      }
      case SEARCH_SUGGESTION:return{
        ...state,
        search: {
          ...state.search,
          suggestions: payload,
          suggestionStatus: "SUCCESS"
        }
      }
      default:
        return state;
    }
  }
  