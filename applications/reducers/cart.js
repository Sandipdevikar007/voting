import {
    LOAD_CART_INIT,
    LOAD_CART,
    LOAD_CART_ERROR
  } from '../actions/types';
  
  const initialState = {
    cartData: {
        data: [],
        status: "NOT STARTED"
    }
  };
  
  export default function(state = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      
      case LOAD_CART_INIT:return{
        ...state,
        cartData:{
            ...state.cartData,
            status: "INITIATED"
        }
      }
      case LOAD_CART:return{
        ...state,
        cartData:{
            ...state.cartData,
            data: payload,
            status: "SUCCESS"
        }
      }
      case LOAD_CART_ERROR:return{
        ...state,
        cartData:{
            ...state.cartData,
            data:[],
            status: "FAILURE"
        }
      }
      default:
        return state;
    }
  }
  