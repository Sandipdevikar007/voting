import React from 'react';
import './style.less';
import ProductItem from '../../components/product-item';
import { connect } from 'react-redux';
import { submitCartItem, loadCart } from '../../../actions/add-to-cart';
import { loadUser, login, logout, register } from '../../../actions/auth';
import { loadProducts, loadProductDetail } from '../../../actions/productlist';
import { scrollElementBelowHeader } from '../../utils/scroll-to-view-element';
import Spinner from '../../../layout/Spinner';
import Toast from '../../components/notification';
import { withRouter, RouteComponentProps } from 'react-router';
import Pagination from '../../components/pagination';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
const options = [

    { value: 'Default', label: 'Default' },
    { value: 'Sort by Name: A - Z', label: 'Sort by Name: A - Z' },
    { value: 'Sort by Name: Z - A', label: 'Sort by Name: Z - A' },
    { value: 'Sort by price: low to high', label: 'Sort by price: low to high' },
    { value: 'Sort by price: high to low', label: 'Sort by price: high to low' }
  ]
interface ILandingPageProps {
    loadUser: () => void;
    logout: () => void;
    loadCart: () => void;
    register: (data) => void;
    loadProducts: (page) => void;
    loadProductDetail: (productId) => void;
    submitCartItem: (products: ISelectedItem) => void;
    login: (data: ILoginData) => void;
}
interface ILoginData {
    email: string;
    password: string
}
interface ISelectedItem {
    product: {
        itemName: string,
        quantity: number,
        unitPrice: number,
        id: string,
        imageUrl: string,
        discount?: number
    }
}
interface IProducts {
    productDetail: IProductDetail
    productList: IProducts,
    search: {
        searchText: string,
        searchFlag: boolean
    }
}
interface IProductDetail {
    data: IProductList,
    status: string
}
interface IProducts {
    data: IProductList[],
    status: string,
    totalRecords: number
}
interface IProductList {
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    platfrom: string,
    _id: string,
    additionalDescription: string,
    description: string,
    additionalImages: string[],
    features: string,
    rating: string,
    discount:number,
    objectID: string
}

interface ISubmitCart{
    submitCartItem: {
        data: object,
        status: string
    }
}
interface IBucket{
    cartData: {
        data: object[],
        status: string
    }
}
interface ILandingPageStateProps {
    products: IProducts,
    auth: IAuth,
    cart: ISubmitCart,
    bucket: IBucket,
    alert: {
        msg: string,
        alertType: string
    }[]
}
interface IAuth {
    token: string,
    isAuthenticated: boolean,
    loading: boolean
    user: IUser,
    status: string
}

interface IUser{
    UserType: string,
    name: string,
    email: string,
}
interface IToastArr{
    id: string,
    description: string,
    type: string,
    time: number
}
interface ILandingPageState {
    selected: number,
    quantity: string,
    name: string,
    password: string,
    email: string,
    toastArr: IToastArr[],
    signupEmail: string,
    signupPassword: string,
    filteredData: IProductList[]
    currentPage: number;
    pageSize: number;
    selectedOption: any
    sortType: string
}
export type LandingPageProps = ILandingPageProps & ILandingPageStateProps & RouteComponentProps<{}>
export class LandingPageComponent extends React.Component<LandingPageProps, ILandingPageState>{
    constructor(props) {
        super(props)
        this.state = {
            selected: -1,
            quantity: '1',
            name: '',
            email: '',
            password: '',
            toastArr: [],
            signupEmail: '',
            signupPassword: '',
            filteredData: [],
            currentPage: 0,
            pageSize: 20,
            selectedOption: null,
            sortType: ''
        }
    }
    private ItemDetailsRef;
    private SignInModalRef;
    private selectYourProductRef;

    public componentDidMount() {
        this.props.loadUser();
        this.props.loadProducts(0);
        this.props.loadCart();
    }
    public componentDidUpdate(prevProps: ILandingPageProps & ILandingPageStateProps, prevState: ILandingPageState) {
        if (prevProps.products.productDetail.status !== this.props.products.productDetail.status &&
            this.props.products.productDetail.status === 'SUCCESS') {
            scrollElementBelowHeader(this.ItemDetailsRef)
        }
    }

    public componentWillReceiveProps(newProps: ILandingPageProps & ILandingPageStateProps) {
        if (newProps.cart.submitCartItem.status !== this.props.cart.submitCartItem.status) {
            if (newProps.cart.submitCartItem.status === 'SUCCESS') {
                const toast: IToastArr[] = [];
                toast.push({
                    id: '12',
                    description: `${newProps.products.productDetail.data.name} added successfully`,
                    type: 'success',
                    time: Date.now()
                },
                )
                this.setState({
                    toastArr: toast,
                    selected: -1,
                    quantity: '1'
                })
            }
            if (newProps.cart.submitCartItem.status === 'FAILURE') {
                const toast: IToastArr[] = [];
                toast.push({
                    id: '12',
                    description: `Please sign in to add item in cart`,
                    type: 'error',
                    time: Date.now()
                },
                )
                this.setState({
                    toastArr: toast
                })
            }
        }
        if (newProps.auth.isAuthenticated !== this.props.auth.isAuthenticated ||
            (newProps.cart.submitCartItem.status !== this.props.cart.submitCartItem.status && newProps.cart.submitCartItem.status === 'SUCCESS')) {
                setTimeout(() => this.props.loadCart(), 50)
        }
        if (newProps.products.productList.status !== this.props.products.productList.status) {
            if (newProps.products.productList.status === 'SUCCESS') {
                this.setState({
                    selected: -1,
                    filteredData: newProps.products.productList.data,
                    currentPage: 0,
                    pageSize: 20,
                })
            }
        }

        if(newProps.auth.status !== this.props.auth.status){
            if(newProps.auth.status === 'FAILURE'){
                if (newProps.alert && newProps.alert[0]) {
                    const toast: IToastArr[] = [];
                    toast.push({
                        id: '12',
                        description: newProps.alert[0].msg,
                        type: 'error',
                        time: Date.now()
                    },
                    )
                    this.setState({
                    toastArr: toast
                })
                }

            }
        }
    }
    public render() {
        const totalPages = Math.ceil(this.props.products.productList.totalRecords / this.state.pageSize);
        return (
            <div className="wb-landing-page">
                <Toast
                    toastList={this.state.toastArr}
                    position={'top-right'}
                    autoDelete={true}
                    autoDeleteTime={3000}
                />
                {(this.props.products.productDetail.status === 'INITIATED' ||
                    this.props.products.productList.status === 'INITIATED' ||
                    this.props.cart.submitCartItem.status === 'INITIATED') && <Spinner />}
                <div className="container">
                   {
                       this.props.products.search.searchFlag &&(<div className="search-result">
                        <div className="showing-result-container">
                        Showing results for <span style={{fontWeight: 'bold'}}>"{this.props.products.search.searchText}"</span>
                        </div>
                            <div className={'show-all'}
                                // tslint:disable-next-line:jsx-no-lambda
                                onClick={() => this.props.loadProducts(0)}>
                                Show all
                        </div>
                        </div>
                       )
                   }
                    <div className="top-title">
                        <div className="page-title page-title-spacing">
                        Books
                        </div>
                        <div className="sort-by-container">
                            {
                                this.props.products.search.searchFlag && <Select
                                    placeholder={"Sort by"}
                                    value={this.state.selectedOption}
                                    name={'names-sort'}
                                    // tslint:disable-next-line:jsx-no-lambda
                                    onChange={(e) => {
                                        this.setState({
                                            selectedOption: e,
                                            sortType: e.value
                                        }, () => this.sortByHandler(this.state.sortType));
                                    }
                                    }
                                    options={options} makeAnimated />
                            }
                        </div>
                    </div>
                    <div className="select-your-book-heading heading24" ref={(node)=>{this.selectYourProductRef = node}}>
                        Select your book
                    </div>
                   
                    <div className="book-container">
                        {
                            this.props.products && this.props.products.productList &&
                            this.state.filteredData.slice(this.state.currentPage * this.state.pageSize,
                                this.state.currentPage * this.state.pageSize + this.state.pageSize).map((product, index) => (
                                <React.Fragment key={index}>
                                        <ProductItem product={product}
                                            isAdmin={(this.props.auth.user && this.props.auth.user.UserType === 'admin') ? true : false}
                                            selected={this.state.selected === index}
                                            updateClick={
                                                // tslint:disable-next-line:jsx-no-lambda
                                                () => {
                                                this.props.loadProductDetail(product.objectID);
                                                this.updateClick()
                                            }}
                                            onClick={
                                            // tslint:disable-next-line:jsx-no-lambda
                                            () => {
                                                this.setState(
                                                    {
                                                        selected: index
                                                    }
                                                )
                                                this.props.loadProductDetail(product.objectID)
                                            }
                                        }
                                    />
                                </React.Fragment>
                            ))
                        }
                    </div>
                    <hr/>
                    <Pagination
                                        pages={totalPages}
                                        pageSize={this.state.pageSize}
                                        pageSizeOptions={["4","25","50"]}
                                        canPrevious={!(this.state.currentPage === 0)}
                                        totalRecordLength={this.props.products.productList.totalRecords}
                                        restorePageNumber={0}
                                        page={this.state.currentPage}
                                        canNext={!(this.state.currentPage === totalPages - 1)}
                                        onPageChange={
                                            // tslint:disable-next-line:jsx-no-lambda
                                            (currentPage) => {
                                                console.log(currentPage)
                                                this.setState({
                                                    currentPage,
                                                    selected: -1,
                                                    
                                                },()=> {
                                                    scrollElementBelowHeader(this.selectYourProductRef)})
                                            }}
                                        onPageSizeChange= {
                                            // tslint:disable-next-line:jsx-no-lambda
                                            (pageSize) => this.setState({ pageSize })}
                                    />
                    <hr />
                    <div className="product-detail-container" ref={(node) => this.ItemDetailsRef = node}>
                        {console.log(this.state.currentPage+"currentPage")}
                        <h3>Configure your product</h3>
                        <br/>
                        {
                            this.state.selected !== -1 &&
                                this.props.products.productDetail.data ?

                                <div className="product-details" style={{ minHeight: '84vh'}}>
                                    <main className="">
                                        <div className="container dark-grey-text">

                                            <div className="row wow fadeIn">

                                                <div className="col-md-6 mb-4">

                                                    <img src={this.props.products.productDetail.data.imageUrl} className="img-fluid" alt="" />

                                                </div>
                                                <div className="col-md-6 mb-4">
                                                    <div className="">
                                                        <div className="mb-3">
                                                            <a href="">
                                                                <span className="badge purple mr-1">Category 2</span>
                                                            </a>
                                                            <a href="">
                                                                <span className="badge blue mr-1">New</span>
                                                            </a>
                                                            <a href="">
                                                                <span className="badge red mr-1">Bestseller</span>
                                                            </a>
                                                        </div>

                                                        <p className="lead">
                                                            <span className="mr-1 heading20">
                                                            {
                                    this.props.products.productDetail.data.discount && <>
                                        <del className="body20">&#8377;{this.props.products.productDetail.data.fromPrice ? this.numberWithCommas(Number(this.props.products.productDetail.data.fromPrice)) : 0}</del>
                                    &nbsp;</>
                                }
                     &#8377;{
                                    this.props.products.productDetail.data.discount ?
                                        (this.props.products.productDetail.data.fromPrice ? this.numberWithCommas(Number(this.props.products.productDetail.data.fromPrice) - (Number(this.props.products.productDetail.data.discount / 100) * this.props.products.productDetail.data.fromPrice)) : 0) : this.props.products.productDetail.data.fromPrice ? this.numberWithCommas(Number(this.props.products.productDetail.data.fromPrice)) : 0}
                            
                                                            </span>
                                                            <span className={'discount heading20 green-success'}> {this.props.products.productDetail.data.discount && this.props.products.productDetail.data.discount+'% off'}</span>
                                                        </p>

                                                        <p className="lead font-weight-bold">Description</p>

                                                        <p>{this.props.products.productDetail.data.description ? this.props.products.productDetail.data.description : this.props.products.productDetail.data.name}</p>

                                                        <div className="d-flex justify-content-left">
                                                            <input type="number" placeholder={'Enter quantity'} name={'quantity'} min="1" value={this.state.quantity} onChange={(e) => {
                                                                e.preventDefault();
                                                                this.setState({ quantity: e.target.value })
                                                            }} className="form-control" />
                                                            <button className="btn btn-primary btn-md my-0 p" onClick={this.submitcartItem} type="button">Add to cart
                <i className="fas fa-shopping-cart ml-1" />
                                                            </button>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <hr/>
                                                {this.props.products.productDetail.data.additionalDescription&&
                                                    <div className="row d-flex justify-content-center wow fadeIn">


                                                    <div className="col-md-6 text-center">

                                                        <h4 className="my-4 h4">Additional information</h4>

                                                        <p>{this.props.products.productDetail.data.additionalDescription}.</p>

                                                    </div>


                                                </div>
                                                }



                                                <div className="row wow fadeIn">

                                                        {
                                                        this.props.products.productDetail.data.additionalImages&&
                                                            this.props.products.productDetail.data.additionalImages.map((url, index)=>(
                                                                <div className="col-lg-4 col-md-12 flex-center mb-4" key={index}>
                                                                <img src={url} className="img-fluid" alt="" />
                                                            </div>
                                                            ))
                                                        }
                                                </div>
                                        </div>
                                    </main>
                                </div> :
                                <div>
                                    No product selected
                            </div>
                        }
                    </div>
                </div>
                <div className="modal fade" id="elegantModalForm" ref={(node) => { this.SignInModalRef = node }} tabIndex={-1} role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content form-elegant">
                            <div className="modal-header text-center">
                                <h3 className="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Sign in</strong></h3>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body mx-4">

                                <div className="md-form mb-5">
                                    <input type="email" id="Form-email1" value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })} className="form-control validate" />
                                    <label data-error="wrong" data-success="right" htmlFor="Form-email1">Your email</label>
                                </div>

                                <div className="md-form pb-3">
                                    <input type="password" id="Form-pass1" value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })} className="form-control validate" />
                                    <label data-error="wrong" data-success="right" htmlFor="Form-pass1">Your password</label>
                                    <p className="font-small blue-text d-flex justify-content-end">Forgot <a href="#" className="blue-text ml-1">
                                        Password?</a></p>
                                </div>

                                <div className="text-center mb-3">
                                    <button type="button" ref={(node) => { this.SignInModalRef = node }} data-dismiss={'modal'} onClick={() => this.props.login({ email: this.state.email, password: this.state.password })} className="btn blue-gradient btn-block btn-rounded z-depth-1a">Sign in</button>
                                </div>
                                <p className="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or Sign in
          with:</p>

                                <div className="row my-3 d-flex justify-content-center">

                                    <button type="button" className="btn btn-white btn-rounded mr-md-3 z-depth-1a"><i className="fab fa-facebook-f text-center"/></button>

                                    <button type="button" className="btn btn-white btn-rounded mr-md-3 z-depth-1a"><i className="fab fa-twitter"/></button>

                                    <button type="button" className="btn btn-white btn-rounded z-depth-1a"><i className="fab fa-google-plus-g"/></button>
                                </div>
                            </div>
                            <div className="modal-footer mx-5 pt-3 mb-1">
                                <p className="font-small grey-text d-flex justify-content-end">Not a member? <span className="blue-text ml-1"
                                data-dismiss="modal" data-toggle="modal" data-target="#elegantModalSignupForm">
                                    Sign Up</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="elegantModalSignupForm" tabIndex={-1} role="dialog" aria-labelledby="myModalSignupLabel"
                    aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content form-elegant">
                            <div className="modal-header text-center">
                                <h3 className="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalSignupLabel"><strong>Sign up</strong></h3>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body mx-4">
                                <div className="md-form mb-5">
                                    <input type="text" id="orangeForm-name" value={this.state.name} onChange={(e) => this.setState({ name: e.target.value })}  className="form-control validate" />
                                    <label data-error="wrong"  data-success="right" htmlFor="orangeForm-name">Your name</label>
                                </div>
                                <div className="md-form mb-5">
                                    <input type="email" id="Form-email1" value={this.state.signupEmail} onChange={(e) => this.setState({ signupEmail: e.target.value })}  className="form-control validate" />
                                    <label data-error="wrong" data-success="right" htmlFor="Form-email1">Your email</label>
                                </div>

                                <div className="md-form pb-3">
                                    <input type="password" id="Form-pass1" value={this.state.signupPassword} onChange={(e) => this.setState({ signupPassword: e.target.value })}  className="form-control validate" />
                                    <label data-error="wrong" data-success="right" htmlFor="Form-pass1">Your password</label>
                                </div>
                                <div className="text-center mb-3">
                                    <button type="button" className="btn blue-gradient btn-block btn-rounded z-depth-1a" data-dismiss={'modal'} onClick={() => this.props.register({ name: this.state.name , email: this.state.signupEmail, password: this.state.signupPassword })} >Sign in</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

    private sortByHandler = (sortType) => {
        let filterData = [...this.state.filteredData];
        switch (sortType) {
            case 'Default':
                this.setState({
                    filteredData: this.props.products.productList.data
                })
                break;
            case 'Sort by Name: Z - A':
                filterData = filterData.sort((prev, next) => {
                    if (prev.name > next.name) {
                        return -1
                    }
                    if (prev.name < next.name) {
                        return 1
                    }
                    return 0
                });
                break;
            case 'Sort by Name: A - Z':
                filterData = filterData.sort((prev, next) => {
                    if (prev.name < next.name) {
                        return -1
                    }
                    if (prev.name > next.name) {
                        return 1
                    }
                    return 0
                });
                break;
            case 'Sort by price: low to high':
                filterData = filterData.sort((prev, next) => prev.fromPrice - next.fromPrice)
                break;
            case 'Sort by price: high to low':
                filterData = filterData.sort((prev, next) => next.fromPrice - prev.fromPrice)
                break;
            case 'Sort by discount: high to low':
                filterData = filterData.sort((prev, next) => next.discount - prev.discount)
                break;
            case 'Sort by discount: low to high':
                filterData = filterData.sort((prev, next) => next.discount - prev.discount)
                break;
        }
        if (sortType !== 'Default') {
            this.setState({
                filteredData: filterData,
                sortType,
                currentPage: 0,
                pageSize: 20,
                selected: -1
            })
        }

    }
    private updateClick = () => {
        if (this.props.history.location.pathname !== '/product-update') {
            this.props.history.push('/product-update');
        }
    }
    private submitcartItem = () => {
        const unitPrice= this.props.products.productDetail.data.discount ? Number(this.props.products.productDetail.data.fromPrice - this.props.products.productDetail.data.discount / 100 * this.props.products.productDetail.data.fromPrice): this.props.products.productDetail.data.fromPrice 
        const item: ISelectedItem = {
            product: {
                itemName: this.props.products.productDetail.data.name,
                quantity: Number(this.state.quantity),
                id: this.props.products.productDetail.data._id,
                unitPrice,
                imageUrl: this.props.products.productDetail.data.imageUrl
            }
        }
        if (Number(this.state.quantity) > 0) {
            this.props.submitCartItem(item);
        }else{
            const toast: IToastArr[] = [];
            toast.push({
                id: '122',
                description: `Quantity should be greater than 0`,
                type: 'error',
                time: Date.now()
            },
            )
            this.setState({
                toastArr: toast
            })
        }
    }
    private numberWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}
const mapStateToProps = (state: ILandingPageStateProps)  => {
    return {
        products: state.products,
        auth: state.auth,
        cart: state.cart,
        bucket: state.bucket,
        alert: state.alert
    }
}
export const mapDispatchToProps = (
    dispatch
): ILandingPageProps => {
    return {
        loadUser: () => dispatch(loadUser()),
        loadProductDetail: (productId) => dispatch(loadProductDetail(productId)),
        submitCartItem: (products) => dispatch(submitCartItem(products)),
        login: (data) => dispatch(login(data)),
        logout: () => dispatch(logout()),
        loadCart: () => dispatch(loadCart()),
        loadProducts: (page) => dispatch(loadProducts(page)),
        register: (data) => dispatch(register(data)),
    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LandingPageComponent))