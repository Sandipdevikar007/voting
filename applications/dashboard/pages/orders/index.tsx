import React from 'react';
import ProductItem from '../../components/product-item';
import { connect } from 'react-redux';
import { submitCartItem, loadCart, submitOrder } from '../../../actions/add-to-cart';
import { loadOrders } from '../../../actions/orders'
import { loadUser, login, logout } from '../../../actions/auth';
import { loadProducts, loadProductDetail } from '../../../actions/productlist';
import { scrollElementBelowHeader } from '../../utils/scroll-to-view-element';
import Spinner from '../../../layout/Spinner';
import Toast from '../../components/notification';
import { withRouter, RouteComponentProps } from 'react-router';
import { Link, Redirect } from 'react-router-dom';

import './style.less'
interface IOrdersProps {
    loadUser: () => void;
    logout: () => void;
    loadCart: () => void;
    loadProducts: () => void;
    submitOrder: (orderDetail: IOrderDetail) => void;
    loadProductDetail: (productId) => void;
    loadOrders: () => void;
}
interface IAuth {
    token: string,
    isAuthenticated: boolean,
    loading: boolean
    user: IUser;
    status: string;
}
interface IUser {
    UserType: string,
    name: string,
    email: string,
}
interface IOrdersStateProps {
    products: IProducts,
    auth: IAuth,
    cart: ISubmitCart,
    bucket: IBucket,
    order: IOrder,
    productOrders: IProductOrders;
}
interface IProductOrders {
    order: {
        data: IOrdersDetail[],
        status: string
    }
}

interface IOrdersDetail {
    deliveryAddress: {
        addressLine1: string;
        addressLine2: string;
        city: string;
        state: string;
        pincode: string;
        mobileNumber: string;
        firstName: string;
        lastName: string;
        country: string
    };
    orderedItems: IOrderedItems[];
    customerName: string;
    totalPrice: number;
}
interface IOrderedItems {
    itemName: string;
    quantity: number;
    unitPrice: number;
    id: string;
    imageUrl: string;
}
interface IOrder {
    submitOrder: {
        data: object,
        status: string
    }
}
interface ILoginData {
    email: string;
    password: string
}
interface ISelectedItem {
    product: {
        itemName: string,
        quantity: number,
        unitPrice: number,
        id: string
    }
}
interface IProducts {
    productDetail: IProductDetail
    productList: IProducts
}
interface IProductDetail {
    data: IProductList,
    status: string
}
interface IProducts {
    data: IProductList[],
    status: string
}
interface IProductList {
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    platfrom: string,
    _id: string
}

interface ISubmitCart {
    submitCartItem: {
        data: object,
        status: string
    }
}
interface IBucket {
    cartData: {
        data: IBucketData[],
        status: string
    }
}
interface IBucketData {
    itemName: string;
    imageUrl: string;
    unitPrice: number;
    quantity: number;
}
interface IOrdersState {
    totalPrice: number;
    firstName: string;
    lastName: string;
    email: string;
    country: string;
    state: string;
    addressLine1: string;
    addressLine2: string;
    pincode: string;
    mobileNumber: string;
    city: string;
}
interface IOrderDetail {
    deliveryAddress: {
        addressLine1: string;
        addressLine2: string;
        city: string;
        state: string;
        pincode: string;
        mobileNumber: string;
        firstName: string;
        lastName: string;
    };
}
export type OrdersProps = IOrdersProps & IOrdersStateProps & RouteComponentProps<{}>;
export class OrdersPage extends React.Component<OrdersProps, IOrdersState>{
    constructor(props) {
        super(props);
        this.state = {
            totalPrice: 0,
            firstName: '',
            lastName: '',
            email: '',
            addressLine1: '',
            addressLine2: '',
            country: '',
            state: '',
            pincode: '',
            mobileNumber: '',
            city: ''
        }
    }

    public componentDidMount() {
        this.props.loadUser();
    }
    public componentWillReceiveProps(newProps: OrdersProps) {
        if (newProps.auth.status === 'FAILURE' || (newProps.auth.user && newProps.auth.user.UserType.toLocaleLowerCase() !== 'admin')) {
            this.props.history.push('/');
        }

        if (newProps.auth.status !== this.props.auth.status) {
            if (newProps.auth.status === 'SUCCESS' || (newProps.auth.user && newProps.auth.user.UserType.toLocaleLowerCase() === 'admin')) {
                this.props.loadOrders();
            }
        }
    }
    public render() {
        return (

            <div className="order-container">
                {(this.props.products.productDetail.status === 'INITIATED' ||
                    this.props.bucket.cartData.status === 'INITIATED' ||
                    this.props.auth.status === 'INITIATED' ||
                    this.props.productOrders.order.status === 'INITIATED') && <Spinner />}
                <div className="mt-4">
                <h4 style={{ textAlign: 'center' }}> <b>Total Orders</b> : {this.props.productOrders.order.data.length}</h4>
                </div>
                {this.props.productOrders.order.data.map((item, index) => (
                    <div className="container-fluid my-5 d-flex justify-content-center" key={index}>
                        <div className="card card-1">
                            <div className="card-body">
                                <div className="row justify-content-between mb-3">
                                    <div className="col-auto">
                                        <h6 className="color-1 mb-0 change-color">Receipt</h6>
                                    </div>
                                    <div className="col-auto "> <small>Receipt Voucher : 1KAU9-84UIL</small> </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="customer-detail">
                                            <h4> <b>Customer details</b></h4>
                                        </div>
                                        <div className="customer-name mb-2">
                                            <span className="mb-1 ml-3"><b>Name:</b></span> {item.customerName}
                </div>
                                        <div className="address mb-2">
                                            <span className="mb-1 ml-3 mr-1"><b>Address:</b></span>

                                            {item.deliveryAddress.addressLine1 + ' ' + item.deliveryAddress.addressLine2 +
                                                ' ,' + item.deliveryAddress.city + ', ' + item.deliveryAddress.state + ', ' + item.deliveryAddress.country + ', ' + item.deliveryAddress.pincode}
                                        </div>
                                        <div className="mobile-number mb-2">
                                            <span className="mb-1 ml-3"><b>Mobile Number:</b></span> {item.deliveryAddress.mobileNumber}
                </div>
                                    </div>
                                </div>
                                <br />
                                <h4 style={{ textAlign: 'center' }}> <b>Items</b></h4>
                                <br />
                                {
                                    item.orderedItems.map((order, indexing) => (
                                        <div className="row mt-4" key={indexing}>
                                            <div className="col">
                                                <div className="card card-2">
                                                    <div className="card-body">
                                                        <div className="media">
                                                            <div className="sq align-self-center "> <img className="img-fluid my-auto align-self-center mr-2 mr-md-4 pl-0 p-0 m-0" src={order.imageUrl} width="135" height="135" /> </div>
                                                            <div className="media-body my-auto text-right">
                                                                <div className="row my-auto flex-column flex-md-row">
                                                                    <div className="col-md-7 col pl-0 my-auto">
                                                                        <h6 className="mb-0"> {order.itemName}</h6>
                                                                    </div>
                                                                    <div className="col my-auto"> <small>Qty : {order.quantity}</small></div>
                                                                    <div className="col my-auto">
                                                                        <h6 className="mb-0">&#8377;{order.unitPrice ? this.numberWithCommas(order.unitPrice): 0}</h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                                <div className="row mt-4">
                                    <div className="col">
                                        <div className="row justify-content-between">
                                            <div className="col-auto">
                                                <p className="mb-1 text-dark"><b>Order Details</b></p>
                                            </div>
                                            <div className="flex-sm-col text-right col">
                                                <p className="mb-1"><b>Total</b></p>
                                            </div>
                                            <div className="flex-sm-col col-auto">
                                                <p className="mb-1">&#8377;{item.totalPrice ? this.numberWithCommas(item.totalPrice): 0}</p>
                                            </div>
                                        </div>
                                        <div className="row justify-content-between">
                                            <div className="flex-sm-col text-right col">
                                                <p className="mb-1"> <b>Discount</b></p>
                                            </div>
                                            <div className="flex-sm-col col-auto">
                                                <p className="mb-1">&#8377;0</p>
                                            </div>
                                        </div>
                                        <div className="row justify-content-between">
                                            <div className="flex-sm-col text-right col">
                                                <p className="mb-1"><b>GST 0%</b></p>
                                            </div>
                                            <div className="flex-sm-col col-auto">
                                                <p className="mb-1">0</p>
                                            </div>
                                        </div>
                                        <div className="row justify-content-between">
                                            <div className="flex-sm-col text-right col">
                                                <p className="mb-1"><b>Delivery Charges</b></p>
                                            </div>
                                            <div className="flex-sm-col col-auto">
                                                <p className="mb-1">Free</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer">
                                <div className="jumbotron-fluid">
                                    <div className="row justify-content-between ">
                                        <div className="col-auto my-auto ">
                                            <h2 className="mb-0 font-weight-bold">TOTAL PAID</h2>
                                        </div>
                                        <div className="col-auto my-auto ml-auto">
                                            <h4 className="display-4 ">&#8377; {item.totalPrice ? this.numberWithCommas(item.totalPrice): 0}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                ))}
            </div>
        )
    }
    private numberWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    private submitOrder = () => {
        const deliveryDetail = {
            deliveryAddress: {
                addressLine1: this.state.addressLine1,
                addressLine2: this.state.addressLine2,
                city: this.state.city,
                state: this.state.state,
                pincode: this.state.pincode,
                mobileNumber: this.state.mobileNumber,
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }
        this.props.submitOrder(deliveryDetail);
    }
}

const mapStateToProps = (state: IOrdersStateProps) => {
    return {
        products: state.products,
        auth: state.auth,
        cart: state.cart,
        bucket: state.bucket,
        order: state.order,
        productOrders: state.productOrders
    }
}

export const mapDispatchToProps = (
    dispatch
): IOrdersProps => {
    return {
        loadUser: () => dispatch(loadUser()),
        loadProductDetail: (productId) => dispatch(loadProductDetail(productId)),
        logout: () => dispatch(logout()),
        loadCart: () => dispatch(loadCart()),
        loadProducts: () => dispatch(loadProducts()),
        submitOrder: () => dispatch(submitOrder()),
        loadOrders: () => dispatch(loadOrders())
    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OrdersPage))