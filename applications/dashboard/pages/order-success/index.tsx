import React from 'react';
import ProductItem from '../../components/product-item';
import { connect } from 'react-redux';
import { submitCartItem, loadCart, submitOrder} from '../../../actions/add-to-cart';
import { loadUser, login, logout } from '../../../actions/auth';
import { loadProducts, loadProductDetail } from '../../../actions/productlist';
import { scrollElementBelowHeader } from '../../utils/scroll-to-view-element';
import Spinner from '../../../layout/Spinner';
import Toast from '../../components/notification';
import { withRouter, RouteComponentProps } from 'react-router';
import {Link, Redirect} from 'react-router-dom';
import './style.less'
interface ICheckoutProps {
    loadUser: () => void;
    logout: () => void;
    loadCart: () => void;
    loadProducts: () => void;
    submitOrder:(orderDetail:IOrderDetail) => void;
    loadProductDetail: (productId) => void;
}
interface IAuth {
    token: string,
    isAuthenticated: boolean,
    loading: boolean
    user: any
}
interface ICheckoutStateProps {
    products: IProducts,
    auth: IAuth,
    cart: ISubmitCart,
    bucket: IBucket
}
interface ILoginData {
    email: string;
    password: string
}
interface ISelectedItem {
    product: {
        itemName: string,
        quantity: number,
        unitPrice: number,
        id: string
    }
}
interface IProducts {
    productDetail: IProductDetail
    productList: IProducts
}
interface IProductDetail {
    data: IProductList,
    status: string
}
interface IProducts {
    data: IProductList[],
    status: string
}
interface IProductList {
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    platfrom: string,
    _id: string
}

interface ISubmitCart {
    submitCartItem: {
        data: object,
        status: string
    }
}
interface IBucket {
    cartData: {
        data: IBucketData[],
        status: string
    }
}
interface IBucketData{
    itemName: string;
    imageUrl: string;
    unitPrice: number;
    quantity: number;
}
interface ICheckoutState{
    totalPrice: number;
    firstName: string;
    lastName: string;
    email: string;
    country: string;
    state: string;
    addressLine1: string;
    addressLine2: string;
    pincode: string;
    mobileNumber: string;
    city: string;
}
interface IOrderDetail{
    deliveryAddress: {
        addressLine1: string;
        addressLine2: string;
        city: string;
        state: string;
        pincode: string;
        mobileNumber: string;
        firstName: string;
        lastName: string;
    };
}
export type OrderSuccessProps = ICheckoutProps & ICheckoutStateProps & RouteComponentProps<{}>;
export class OrderSuccess extends React.Component<OrderSuccessProps, ICheckoutState>{
    constructor(props) {
        super(props);
        this.state = {
            totalPrice: 0,
            firstName: '',
            lastName: '',
            email: '',
            addressLine1:'',
            addressLine2: '',
            country: '',
            state: '',
            pincode: '',
            mobileNumber: '',
            city: ''
        }
    }

    public componentDidMount(){
        this.props.loadCart();
        this.props.loadUser();
    }
    public componentWillReceiveProps(newProps: OrderSuccessProps){
        if (newProps.bucket.cartData.status !== this.props.bucket.cartData.status && !newProps.auth.isAuthenticated) {
            this.props.history.push('/');
        }
        if (newProps.bucket.cartData.status !== this.props.bucket.cartData.status) {
            let price = 0;
            newProps.bucket.cartData.data.forEach((item) => {
                price = price + item.unitPrice * item.quantity
            });
            this.setState({
                totalPrice: price
            })
        }
    }
    public render() {
        return (
            <div className="order-success-container">
                <div className="card">
                    <div className="success">
                        <i className="checkmark">✓</i>
                    </div>
                    <h1>Success</h1>
                    <p>We received your purchase request;<br /> we'll be in touch shortly!</p>
                    <br/>
                    <br/>
                    <button className="btn btn-primary cart-btn-transform m-3" onClick={()=> this.props.history.push('/')} data-abc="true">continue shopping</button>
                </div>
               
            </div>

        )
    }
    private submitOrder = () =>{
        const deliveryDetail = {
            deliveryAddress: {
                addressLine1: this.state.addressLine1,
                addressLine2: this.state.addressLine2,
                city: this.state.city,
                state: this.state.state,
                pincode: this.state.pincode,
                mobileNumber: this.state.mobileNumber,
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }
        this.props.submitOrder(deliveryDetail);
    }
}

const mapStateToProps = (state: ICheckoutStateProps)  => {
    return {
        products: state.products,
        auth: state.auth,
        cart: state.cart,
        bucket: state.bucket
    }
}

export const mapDispatchToProps = (
    dispatch
): ICheckoutProps => {
    return {
        loadUser: () => dispatch(loadUser()),
        loadProductDetail: (productId) => dispatch(loadProductDetail(productId)),
        logout: () => dispatch(logout()),
        loadCart: () => dispatch(loadCart()),
        loadProducts: () => dispatch(loadProducts()),
        submitOrder:() => dispatch(submitOrder()),
    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OrderSuccess))