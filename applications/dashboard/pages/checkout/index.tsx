import React from 'react';
import ProductItem from '../../components/product-item';
import { connect } from 'react-redux';
import { submitCartItem, loadCart, submitOrder} from '../../../actions/add-to-cart';
import { loadUser, login, logout } from '../../../actions/auth';
import { loadProducts, loadProductDetail } from '../../../actions/productlist';
import { scrollElementBelowHeader } from '../../utils/scroll-to-view-element';
import Spinner from '../../../layout/Spinner';
import Toast from '../../components/notification';
import { withRouter, RouteComponentProps } from 'react-router';
import {Link, Redirect} from 'react-router-dom';

import './style.less'
interface ICheckoutProps {
    loadUser: () => void;
    logout: () => void;
    loadCart: () => void;
    loadProducts: () => void;
    submitOrder:(orderDetail:IOrderDetail) => void;
    loadProductDetail: (productId) => void;
}
interface IAuth {
    token: string,
    isAuthenticated: boolean,
    loading: boolean
    user: any;
    status: string;
}
interface ICheckoutStateProps {
    products: IProducts,
    auth: IAuth,
    cart: ISubmitCart,
    bucket: IBucket,
    order: IOrder
}

interface IOrder {
    submitOrder: {
        data: object,
        status: string
    }
}
interface ILoginData {
    email: string;
    password: string
}
interface ISelectedItem {
    product: {
        itemName: string,
        quantity: number,
        unitPrice: number,
        id: string
    }
}
interface IProducts {
    productDetail: IProductDetail
    productList: IProducts
}
interface IProductDetail {
    data: IProductList,
    status: string
}
interface IProducts {
    data: IProductList[],
    status: string
}
interface IProductList {
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    platfrom: string,
    _id: string
}

interface ISubmitCart {
    submitCartItem: {
        data: object,
        status: string
    }
}
interface IBucket {
    cartData: {
        data: IBucketData[],
        status: string
    }
}
interface IBucketData{
    itemName: string;
    imageUrl: string;
    unitPrice: number;
    quantity: number;
}
interface ICheckoutState{
    totalPrice: number;
    firstName: string;
    lastName: string;
    email: string;
    country: string;
    state: string;
    addressLine1: string;
    addressLine2: string;
    pincode: string;
    mobileNumber: string;
    city: string;
}
interface IOrderDetail{
    deliveryAddress: {
        addressLine1: string;
        addressLine2: string;
        city: string;
        state: string;
        pincode: string;
        mobileNumber: string;
        firstName: string;
        lastName: string;
    };
}
export type CheckoutProps = ICheckoutProps & ICheckoutStateProps & RouteComponentProps<{}>;
export class Checkout extends React.Component<CheckoutProps, ICheckoutState>{
    constructor(props) {
        super(props);
        this.state = {
            totalPrice: 0,
            firstName: '',
            lastName: '',
            email: '',
            addressLine1:'',
            addressLine2: '',
            country: '',
            state: '',
            pincode: '',
            mobileNumber: '',
            city: ''
        }
    }

    public componentDidMount(){
        this.props.loadUser();
    }
    public componentWillReceiveProps(newProps: CheckoutProps){
        if (newProps.auth.status !== this.props.auth.status) {
            if (newProps.auth.status === 'SUCCESS') {
                this.props.loadCart();
            }
        }
        if (newProps.bucket.cartData.status !== this.props.bucket.cartData.status) {
            if (newProps.bucket.cartData.status === 'SUCCESS' && newProps.bucket.cartData.data.length === 0) {
                this.props.history.push('/emptycart');
            }
        }
        if (newProps.bucket.cartData.status !== this.props.bucket.cartData.status) {
            let price = 0;
            newProps.bucket.cartData.data.forEach((item) => {
                price = price + item.unitPrice * item.quantity
            });
            this.setState({
                totalPrice: price
            })
        }
        if(this.props.order.submitOrder.status !== newProps.order.submitOrder.status){
            if(newProps.order.submitOrder.status === 'SUCCESS'){
                this.props.history.push('/ordersuccess');
            }
        }
    }
    public render() {
        return (
<main className="">
  <div className="container wow fadeIn">

        {this.props.order.submitOrder.status === 'INITIATED' && <Spinner/>}
    <h2 className="mb-5 mt-2 h2 text-center">Checkout</h2>


    <div className="row">


      <div className="col-md-8 mb-4">


        <div className="card">


          <form className="card-body">


            <div className="row">


              <div className="col-md-6 mb-2">


                <div className="md-form ">
                  <input type="text" id="firstName" value={this.state.firstName} onChange={(e)=> this.setState({firstName: e.target.value})} className="form-control" required />
                  <label htmlFor="firstName" className="">First name</label>
                </div>

              </div>



              <div className="col-md-6 mb-2">


                <div className="md-form">
                  <input type="text" id="lastName" value={this.state.lastName} onChange={(e)=> this.setState({lastName: e.target.value})} className="form-control" required />
                  <label htmlFor="lastName" className="">Last name</label>
                </div>

              </div>


            </div>



            <div className="md-form input-group pl-0 mb-5">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">@</span>
              </div>
              <input type="text" className="form-control py-0" value={this.state.mobileNumber} onChange={(e)=> this.setState({mobileNumber: e.target.value})} placeholder="Mobile Number" aria-describedby="basic-addon1" required/>
            </div>


            <div className="md-form mb-5">
              <input type="text" id="email" value={this.state.email} onChange={(e)=> this.setState({email: e.target.value})}  className="form-control" />
              <label htmlFor="email" className="">Email (optional)</label>
            </div>


            <div className="md-form mb-5">
              <input type="text" id="address" className="form-control" required value={this.state.addressLine1} onChange={(e)=> this.setState({addressLine1: e.target.value})}/>
              <label htmlFor="address" className="">Address</label>
            </div>


            <div className="md-form mb-5">
              <input type="text" id="address-2" value={this.state.addressLine2} onChange={(e)=> this.setState({addressLine2: e.target.value})}  className="form-control"/>
              <label htmlFor="address-2" className="">Address 2 (optional)</label>
            </div>
            <div className="md-form mb-5">
              <input type="text" id="city" value={this.state.city} onChange={(e)=> this.setState({city: e.target.value})}  className="form-control"/>
              <label htmlFor="city" className="">City</label>
            </div>

            <div className="row">


              <div className="col-lg-4 col-md-12 mb-4">

                <label htmlFor="country">Country</label>
                <select className="custom-select d-block w-100" value={this.state.country} onChange={(e)=> this.setState({country: e.target.value})} id="country" required>
                  <option value="">Choose...</option>
                  <option>India</option>
                </select>
                <div className="invalid-feedback">
                  Please select a valid country.
                  </div>

              </div>



              <div className="col-lg-4 col-md-6 mb-4">

                <label htmlFor="state">State</label>
                <select className="custom-select d-block w-100" value={this.state.state} onChange={(e)=> this.setState({state: e.target.value})}  id="state" required>
                  <option value="">Choose...</option>
                  <option>Maharashtra</option>
                </select>
                <div className="invalid-feedback">
                  Please provide a valid state.
                  </div>

              </div>



              <div className="col-lg-4 col-md-6 mb-4">

                <label htmlFor="zip">Zip</label>
                <input type="text" className="form-control" value={this.state.pincode} onChange={(e)=> this.setState({pincode: e.target.value})}  id="zip" placeholder="" required />
                <div className="invalid-feedback">
                  Zip code required.
                  </div>

              </div>


            </div>


            <hr />

            <div className="custom-control custom-checkbox">
              <input type="checkbox" className="custom-control-input" id="same-address" />
              <label className="custom-control-label" htmlFor="same-address">Shipping address is the same as my billing address</label>
            </div>
            <div className="custom-control custom-checkbox">
              <input type="checkbox" className="custom-control-input" id="save-info" />
              <label className="custom-control-label" htmlFor="save-info">Save this information htmlFor next time</label>
            </div>

            <hr />

            <div className="d-none my-3">
              <div className="custom-control custom-radio">
                <input id="credit" name="paymentMethod" type="radio" className="custom-control-input" checked required />
                <label className="custom-control-label" htmlFor="credit">Credit card</label>
              </div>
              <div className="custom-control custom-radio">
                <input id="debit" name="paymentMethod" type="radio" className="custom-control-input" required />
                <label className="custom-control-label" htmlFor="debit">Debit card</label>
              </div>
              <div className="custom-control custom-radio">
                <input id="paypal" name="paymentMethod" type="radio" className="custom-control-input" required />
                <label className="custom-control-label" htmlFor="paypal">Paypal</label>
              </div>
            </div>
            <div className="row d-none">
              <div className="col-md-6 mb-3">
                <label htmlFor="cc-name">Name on card</label>
                <input type="text" className="form-control" id="cc-name" placeholder="" required />
                <small className="text-muted">Full name as displayed on card</small>
                <div className="invalid-feedback">
                  Name on card is required
                  </div>
              </div>
              <div className="col-md-6 mb-3">
                <label htmlFor="cc-number">Credit card number</label>
                <input type="text" className="form-control" id="cc-number" placeholder="" required />
                <div className="invalid-feedback">
                  Credit card number is required
                  </div>
              </div>
            </div>
            <div className="row d-none">
              <div className="col-md-3 mb-3">
                <label htmlFor="cc-expiration">Expiration</label>
                <input type="text" className="form-control" id="cc-expiration" placeholder="" required />
                <div className="invalid-feedback">
                  Expiration date required
                  </div>
              </div>
              <div className="col-md-3 mb-3">
                <label htmlFor="cc-expiration">CVV</label>
                <input type="text" className="form-control" id="cc-cvv" placeholder="" required />
                <div className="invalid-feedback">
                  Security code required
                  </div>
              </div>
            </div>
            <hr className="mb-4" />
            <button className="btn btn-primary btn-lg btn-block" onClick={this.submitOrder} type="submit">Submit Order</button>

          </form>

        </div>


      </div>



      <div className="col-md-4 mb-4">


        <h4 className="d-flex justify-content-between align-items-center mb-3">
          <span className="text-muted">Your cart</span>
        <span className="badge badge-secondary badge-pill">{this.props.bucket.cartData.data.length}</span>
        </h4>


        <ul className="list-group mb-3 z-depth-1">
                                {
                                    this.props.bucket.cartData.data.map((item, index) => (
                                        <li className="list-group-item d-flex justify-content-between lh-condensed" key={index}>
                                            <div>
                                    <h6 className="my-0">{item.itemName}</h6>
                                                <small className="text-muted"></small>
                                            </div>
                                            <span className="text-muted">{this.numberWithCommas(item.quantity * item.unitPrice)}</span>
                                        </li>
                                    ))
                                }
          <li className="list-group-item d-flex justify-content-between">
            <span>Total (INR)</span>
                            <strong>Rs {this.state.totalPrice}</strong>
          </li>
        </ul>



        <form className="card p-2">
          <div className="input-group">
            <input type="text" className="form-control" placeholder="Promo code" aria-label="Recipient's username" aria-describedby="basic-addon2" />
            <div className="input-group-append">
              <button className="btn btn-secondary btn-md waves-effect m-0" type="button">Redeem</button>
            </div>
          </div>
        </form>


      </div>


    </div>


  </div>
</main>

            )
    }
    private submitOrder = () =>{
      // const errorMessages = this.state.errorMessages;
      let validationSucceeded = true;
      // recepient name
      if (!this.state.addressLine1) {
        // errorMessages.recipientName = genericRequiredFieldErrorMessage;
        validationSucceeded = false;
      }
      if (!this.state.mobileNumber) {
        // errorMessages.recipientName = genericRequiredFieldErrorMessage;
        validationSucceeded = false;
      }
      if (!this.state.lastName) {
        // errorMessages.recipientName = genericRequiredFieldErrorMessage;
        validationSucceeded = false;
      }
      if (!this.state.firstName) {
        // errorMessages.recipientName = genericRequiredFieldErrorMessage;
        validationSucceeded = false;
      }
      if (!this.state.pincode) {
        // errorMessages.recipientName = genericRequiredFieldErrorMessage;
        validationSucceeded = false;
      }
      const deliveryDetail = {
        deliveryAddress: {
          addressLine1: this.state.addressLine1,
          addressLine2: this.state.addressLine2,
          city: this.state.city,
          state: this.state.state,
          pincode: this.state.pincode,
          mobileNumber: this.state.mobileNumber,
          firstName: this.state.firstName,
          lastName: this.state.lastName
        }
      }
      if (validationSucceeded) {
        console.log(validationSucceeded+"teu")
        this.props.submitOrder(deliveryDetail);
      }
    }
    private numberWithCommas = (x) => {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}

const mapStateToProps = (state: ICheckoutStateProps)  => {
    return {
        products: state.products,
        auth: state.auth,
        cart: state.cart,
        bucket: state.bucket,
        order: state.order
    }
}

export const mapDispatchToProps = (
    dispatch
): ICheckoutProps => {
    return {
        loadUser: () => dispatch(loadUser()),
        loadProductDetail: (productId) => dispatch(loadProductDetail(productId)),
        logout: () => dispatch(logout()),
        loadCart: () => dispatch(loadCart()),
        loadProducts: () => dispatch(loadProducts()),
        submitOrder: (deliveryDetail) => dispatch(submitOrder(deliveryDetail)),
    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Checkout))