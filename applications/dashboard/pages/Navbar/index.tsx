import React from 'react';
import ProductItem from '../../components/product-item';
import { connect } from 'react-redux';
import { submitCartItem, loadCart } from '../../../actions/add-to-cart';
import { loadUser, login, logout } from '../../../actions/auth';
import { loadProducts, loadProductDetail, searchProducts, searchSuggestions } from '../../../actions/productlist';
import { scrollElementBelowHeader } from '../../utils/scroll-to-view-element';
import Spinner from '../../../layout/Spinner';
import Toast from '../../components/notification';
import { withRouter, RouteComponentProps } from 'react-router';
import {Link, Redirect} from 'react-router-dom';
import './style.less'
import BackDrop from '../../components/backdrop/Backdrop';
import Toolbar from '../../components/toolbar/Toolbar';
import SideDrawer from '../../components/sidedrawer/SideDrawer';
interface INavbarProps {
    loadUser: () => void;
    logout: () => void;
    loadCart: () => void;
    loadProducts: () => void;
    loadProductDetail: (productId) => void;
    searchProducts: (param) => void;
    searchSuggestions: (param) => void;
}
interface IAuth {
    token: string,
    isAuthenticated: boolean,
    loading: boolean
    user: IUser
}
interface IUser{
    UserType: string,
    name: string,
    email: string,
}
interface INavbarStateProps {
    products: IProducts,
    auth: IAuth,
    cart: ISubmitCart,
    bucket: IBucket
}
interface ILoginData {
    email: string;
    password: string
}
interface ISelectedItem {
    product: {
        itemName: string,
        quantity: number,
        unitPrice: number,
        id: string
    }
}
interface IProducts {
    productDetail: IProductDetail
    productList: IProducts
    search: {
        searchText: string,
        searchFlag: boolean,
        suggestions: {
            name: string,
            author?: string,
            categories?: string[],
        }[],
        suggestionStatus: string
    }
}
interface IProductDetail {
    data: IProductList,
    status: string
}
interface IProducts {
    data: IProductList[],
    status: string
}
interface IProductList {
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    platfrom: string,
    _id: string
}

interface ISubmitCart {
    submitCartItem: {
        data: object,
        status: string
    }
}
interface IBucket {
    cartData: {
        data: IBucketData[],
        status: string
    }
}
interface IBucketData{
    itemName: string;
    imageUrl: string;
    unitPrice: number;
    quantity: number;
}
interface INavbarState{
    totalPrice: number;
    sideDrawerOpen: boolean;
    searchText: string;
    searchSuggestions: string[];
    isOpen: boolean;
    activeIndex: number;
}
export type NavbarProps = INavbarProps & INavbarStateProps & RouteComponentProps<{}>;
export class Navbar extends React.Component<NavbarProps, INavbarState>{
    constructor(props) {
        super(props);
        this.state = {
            totalPrice: 0,
            sideDrawerOpen: false,
            searchText: '',
            searchSuggestions: [],
            isOpen: true,
            activeIndex: -1
        }
    }

    public componentWillReceiveProps(newProps: NavbarProps){
        if (newProps.products.search.suggestionStatus !== this.props.products.search.suggestionStatus && this.state.searchText !== '') {
            const suggestions: string[] = []
            newProps.products.search.suggestions.forEach((item) => {
                const searchText = this.state.searchText.toLocaleLowerCase().trim();
                const name = item.name.toLocaleLowerCase().trim();
                const author = item.author && item.author.toLocaleLowerCase().trim();
                if (item.categories && item.categories.length > 0) {
                    item.categories.forEach((i) => {
                        const cat = i.toLocaleLowerCase().trim();
                        if (cat && cat.startsWith(searchText)) {
                            if (cat.length > searchText.length && suggestions.indexOf(cat) === -1) {
                                suggestions.push(cat)
                            }
                        }
                    })
                }
                if (item.name && name.startsWith(searchText)) {
                    if (suggestions.indexOf(name) === -1) {
                        suggestions.push(name)
                    }
                } else if (author && author.startsWith(searchText)) {
                    if (suggestions.indexOf(author) === -1) {
                        suggestions.push(author)
                    }
                }
            })
            this.setState({
                searchSuggestions: suggestions,
            })
        }
        if (newProps.products.productList.status !== this.props.products.productList.status) {
            if (newProps.products.productList.status === 'SUCCESS') {
                this.setState({
                    searchSuggestions: []
                })
                if (this.props.history.location.pathname !== '/') {
                    this.props.history.push('/');
                }
            }
        }
    }
    public componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    public componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    private handleClickOutside = (event) => {

        if (window.innerWidth < 567) {
            const parentElem = document.getElementById('mobile-suggestions-container');
            if (parentElem && !parentElem.contains(event.target)) {
                this.closeDropdown();
            }
        } else {
            const parentElem = document.getElementById('suggestion-container');
            if (parentElem && !parentElem.contains(event.target)) {
                this.closeDropdown();
            }
        }
    }
    private closeDropdown = () => {
        if (this.state.isOpen) {
            const searchText = ''
            const event = { target: { value: searchText } }
            this.setState({ isOpen: false });
        }
    }
    public render() {
        let backdrop;
        if (this.state.sideDrawerOpen) {

            backdrop = <BackDrop click={this.backDropClickhandler} />
        }
        return (
            <>
                <div style={{ height: '0px' }} className={'d-block d-sm-none'}>
                    <Toolbar onSearchTextChange= {this.onSearchTextChange} isOpen={this.state.isOpen}  clearSearch={this.clearSearch} onKeyPress={this.onKeyPress} searchSuggestions={this.state.searchSuggestions} onSearchProduct={this.onSearchProduct} searchText={this.state.searchText} DrawerClickHandler={this.drawerToggleClickHandler} shopCartClick={this.shopCartClick}/>
                    <SideDrawer backDropClickhandler={this.backDropClickhandler} onHomePageGo={this.onHomePageGo} show={this.state.sideDrawerOpen} addBookClick={this.addBookClick} ordersClick={this.ordersClick} shopCartClick={this.shopCartClick} />
                    {backdrop}
                </div>
                <div className="d-none d-sm-block">
                    <nav className="navbar fixed-top navbar-expand-md navbar-light">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav1"
                            aria-controls="basicExampleNav1" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon" />
                        </button>
                        <a className="navbar-author" href="#!">
                            <img src="https://mdbootstrap.com/img/logo/mdb-transaprent-noshadows.png" onClick={this.onHomePageGo} height="30" alt="mdb logo" />
                        </a>
                        <div className="nav-search-container col-md-6">
                            <form className="input-group md-form form-sm form-2 pl-0" onSubmit={this.onSearchProduct}>

                                    <input className="form-control my-0 py-1 red-border" value={this.state.searchText} onKeyDown={this.onKeyPress} onChange={this.onSearchTextChange} type="text" autoComplete={''} placeholder="Search" aria-label="ssearch" />
                                {
                                    this.state.searchText &&
                                    <div className="input-group-append">
                                        <span className="input-group-text cursor-pointer search-clear" onClick={this.clearSearch} id="basic-text1"><i className="fas fa-times text-grey"
                                            aria-hidden="true"/></span>
                                    </div>
                                }
                                    <div className="input-group-append">
                                        <span className="input-group-text cursor-pointer red lighten-3"  onClick={this.onSearchProduct} id="basic-text1"><i className="fas fa-search text-grey"
                                            aria-hidden="true"/></span>
                                    </div>
                        </form>
                        {   this.state.isOpen &&
                            this.state.searchSuggestions &&
                            this.state.searchSuggestions.length > 0 &&
                                (
                                <div className="suggestions-container" id={'suggestion-container'}>
                                    {this.state.searchSuggestions.map((item, index) => (
                                        <div key={index}>
                                            <div className={`cursor-pointer suggestion-item ${this.state.activeIndex === index ? 'active' : ''} `} onClick={
                                                // tslint:disable-next-line:jsx-no-lambda
                                                () => this.onSearchTextChange({
                                                target: {
                                                    value: item,
                                                    suggestionClick: true
                                                }
                                            })}>{this.highlightMatch(item, index)}</div>
                                            </div>
                                        ))}

                                    </div>
                                )
                            }
                        </div>
                        <span className="nav-link navbar-link-2 waves-effect d-block d-sm-none ml-auto" onClick={this.shopCartClick}>
                            <span className="badge badge-pill red">{this.props.bucket.cartData.data &&
                                this.props.bucket.cartData.data.length !== 0 && this.props.bucket.cartData.data.length}</span>
                            <i className="fas fa-shopping-cart pl-0" />
                        </span>

                        <div className="collapse navbar-collapse" id="basicExampleNav1">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item">
                                    <span className="nav-link navbar-link-2 waves-effect d-none d-sm-block " onClick={this.shopCartClick}>
                                        <span className="badge badge-pill red">{this.props.bucket.cartData.data &&
                                            this.props.bucket.cartData.data.length !== 0 && this.props.bucket.cartData.data.length}</span>
                                        <i className="fas fa-shopping-cart pl-0" />
                                    </span>
                                </li>
                                {
                                    this.props.auth.user && this.props.auth.user.UserType &&
                                    this.props.auth.user.UserType.toLocaleLowerCase() === 'admin' &&
                                    <li className="nav-item">
                                        <span data-toggle="collapse" data-target="#basicExampleNav1" onClick={this.ordersClick} className="nav-link waves-effect">
                                            Orders
</span>
                                    </li>
                                }
                                <li className="nav-item" onClick={this.onHomePageGo}>
                                    <span className="nav-link waves-effect">
                                        Shop
</span>
                                </li>
                                <li className="nav-item">
                                    <span className="nav-link waves-effect">
                                        Contact
</span>
                                </li>
                                {!this.props.auth.isAuthenticated && <li className="nav-item">
                                    <span className="nav-link waves-effect" data-toggle="modal" data-target="#elegantModalForm">
                                        Sign in
</span>
                                </li>}
                                {this.props.auth.isAuthenticated && <li className="nav-item">
                                    <span className="nav-link waves-effect" onClick={
                                        // tslint:disable-next-line:jsx-no-lambda
                                        () => this.props.logout()}>
                                        Log out
</span>
                                </li>}
                                {!this.props.auth.isAuthenticated && (
                                    <li className="nav-item pl-2 mb-2 mb-md-0">
                                        <button type="button"
                                            className="btn btn-outline-info btn-md btn-rounded btn-navbar waves-effect waves-light"
                                            data-toggle="modal" data-target="#elegantModalSignupForm">Sign up</button>
                                    </li>)
                                }
                            </ul>
                        </div>
                        {console.log(this.state.activeIndex)}
                    </nav>
                </div></>)
    }
    private ordersClick = () => {
        this.backDropClickhandler();
        if (this.props.history.location.pathname !== '/devivery-order') {
            this.props.history.push('delivery-order');
        }
    }

    private clearSearch = () => {
        this.setState({
            searchText: ''
        })
    }
    private onKeyPress = (e) => {
        if (e.which === 40) {
            if (this.state.activeIndex < this.state.searchSuggestions.length - 1) {
                let index = this.state.activeIndex;
                index = index + 1;
                this.setState({
                    activeIndex: index,
                    searchText: this.state.searchSuggestions[index]
                })
            }else{
                this.setState({
                    activeIndex: 0,
                    searchText: this.state.searchSuggestions[0]
                })
            }
        }
        if (e.which === 38) {
            if (this.state.activeIndex > 0) {
                let index = this.state.activeIndex;
                index = index - 1;
                this.setState({
                    activeIndex: index,
                    searchText: this.state.searchSuggestions[index]
                })
            } else {
                this.setState({
                    activeIndex: this.state.searchSuggestions.length - 1,
                    searchText: this.state.searchSuggestions[this.state.searchSuggestions.length - 1]
                })
            }
        }
        if ((e.which === 32 &&  e.which !== 8) && this.state.searchText !== '') {
            this.props.searchSuggestions({ query: this.state.searchText.slice(0, -1) })
        }
    }
    private onSearchTextChange = (event) => {

        if (event.target.value === ''){
            this.setState({
                searchSuggestions: []
            })
        }
        this.setState({
            searchText: event.target.value,
            isOpen: true
        }, () => {
                if (this.state.searchText) {
                    if (event.target && event.target.suggestionClick){
                        {
                            this.setState({
                                isOpen: false
                            })
                        }
                        this.props.searchProducts({ param: this.state.searchText })
                    }
                    if (this.state.searchText.length == 1 || this.state.searchText.length == 2) {
                        this.props.searchSuggestions({ query: this.state.searchText })
                    }
            }else {
                this.setState({
                    searchSuggestions: []
                })
            }
        })

    }
    private onHomePageGo = () => {
        this.backDropClickhandler();
        if (this.props.history.location.pathname !== '/') {
            this.props.history.push('/');
        }
    }
    private highlightMatch(label: string, itemId) {
        try {
            const match = label.toLowerCase().match(this.state.searchText.toLowerCase());
            if (match != null && match.index !== undefined) {
                const start = label.substring(0, match.index);
                const matchedText = label.substring(match.index, match.index + this.state.searchText.length);
                const end = label.substring(match.index + this.state.searchText.length);
                return (
                    <span id={`${itemId}-text`} data-item={label}>
                        {matchedText}
                        <span id={`${itemId}-highlight`} className="tl-dropdown-match" data-item={label}>
                            {end}
                        </span>

                    </span>
                )
            } else {
                return <span id={`${itemId}-text`} data-item={label}>{label}</span>
            }
        } catch (err) {

        }
    }

    private onSearchProduct = (e) => {
        e.preventDefault();
        if (this.state.searchText && this.state.activeIndex === -1) {
            this.props.searchProducts({ param: this.state.searchText })
        } else {
            this.setState({
                searchText: this.state.searchSuggestions[this.state.activeIndex]
            });
            this.props.searchProducts({ param: this.state.searchSuggestions[this.state.activeIndex] });
        }
        if (this.props.history.location.pathname !== '/') {
            this.props.history.push('/');
        }
    }
    private addBookClick = () => {
        this.backDropClickhandler();
        if (this.props.history.location.pathname !== '/product-upload') {
            this.props.history.push('/product-upload');
        }
    }
    private shopCartClick = () => {
        if (this.props.history.location.pathname !== '/order-summary') {
            this.props.history.push('/order-summary');
        }
    }
    private drawerToggleClickHandler = () => {
        this.setState((prevState) => {
          return { sideDrawerOpen: !prevState.sideDrawerOpen }
        });
      }
    private backDropClickhandler = () => {
        this.setState({ sideDrawerOpen: false });
      }
}

const mapStateToProps = (state: INavbarStateProps)  => {
    return {
        products: state.products,
        auth: state.auth,
        cart: state.cart,
        bucket: state.bucket,
    }
}

export const mapDispatchToProps = (
    dispatch
): INavbarProps => {
    return {
        loadUser: () => dispatch(loadUser()),
        loadProductDetail: (productId) => dispatch(loadProductDetail(productId)),
        logout: () => dispatch(logout()),
        loadCart: () => dispatch(loadCart()),
        loadProducts: () => dispatch(loadProducts()),
        searchProducts: (param) => dispatch(searchProducts(param)),
        searchSuggestions: (param) => dispatch(searchSuggestions(param)),

    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar))