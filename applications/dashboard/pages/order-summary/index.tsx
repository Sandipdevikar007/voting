import React from 'react';
import './style.less';
import ProductItem from '../../components/product-item';
import { connect } from 'react-redux';
import { submitCartItem, loadCart } from '../../../actions/add-to-cart';
import { loadUser, login, logout } from '../../../actions/auth';
import { removeItem} from '../../../actions/remove-item';

import { loadProducts, loadProductDetail } from '../../../actions/productlist';
import { scrollElementBelowHeader } from '../../utils/scroll-to-view-element';
import Spinner from '../../../layout/Spinner';
import Toast from '../../components/notification';
import { withRouter, RouteComponentProps } from 'react-router';
import { Link, Redirect } from 'react-router-dom';
interface IOrderSummaryProps {
    loadUser: () => void;
    logout: () => void;
    removeItem: (itemDetail) => void;
    loadCart: () => void;
    loadProducts: () => void;
    loadProductDetail: (productId) => void;
}
interface IAuth {
    token: string,
    isAuthenticated: boolean,
    loading: boolean
    user: any,
    status: string
}
interface IOrderSummaryStateProps {
    products: IProducts,
    auth: IAuth,
    cart: ISubmitCart,
    bucket: IBucket,
    removeItemData: IRemoveItemData
}

interface IRemoveItemData{
    removeData: {
        data: object[],
        status: string
    }
}
interface ILoginData {
    email: string;
    password: string
}
interface ISelectedItem {
    product: {
        itemName: string,
        quantity: number,
        unitPrice: number,
        id: string
    }
}
interface IProducts {
    productDetail: IProductDetail
    productList: IProducts
}
interface IProductDetail {
    data: IProductList,
    status: string
}
interface IProducts {
    data: IProductList[],
    status: string
}
interface IProductList {
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    platfrom: string,
    _id: string
}

interface ISubmitCart {
    submitCartItem: {
        data: object,
        status: string
    }
}
interface IBucket {
    cartData: {
        data: IBucketData[],
        status: string
    }
}
interface IBucketData {
    itemName: string;
    imageUrl: string;
    unitPrice: number;
    quantity: number;
    id: string
}
interface IOrderSummaryState {
    totalPrice: number
}
export type OrderSummaryProps = IOrderSummaryProps & IOrderSummaryStateProps & RouteComponentProps<{}>;
export class OrderSummary extends React.Component<OrderSummaryProps, IOrderSummaryState>{
    constructor(props) {
        super(props);
        this.state = {
            totalPrice: 0
        }
    }
    public componentDidMount() {
        this.props.loadUser();
        this.props.loadCart()
    }
    public componentWillReceiveProps(newProps: OrderSummaryProps) {
        if (newProps.auth.status === 'FAILURE') {
            this.props.history.push('/');
        }
        if (newProps.bucket.cartData.status !== this.props.bucket.cartData.status) {
            if (newProps.bucket.cartData.status === 'SUCCESS' && newProps.bucket.cartData.data.length === 0) {
                this.props.history.push('/emptycart');
            }
        }
        if (newProps.bucket.cartData.status !== this.props.bucket.cartData.status) {
            let price = 0;
            newProps.bucket.cartData.data.forEach((item) => {
                price = price + item.unitPrice * item.quantity
            });
            this.setState({
                totalPrice: price
            })
        }

        if (newProps.removeItemData.removeData.status !== this.props.removeItemData.removeData.status) {
            if (newProps.removeItemData.removeData.status === 'SUCCESS') {
                this.props.loadCart()
            }
        }
    }
    public render() {
        return (
            <div className="container order-summary">
                {(this.props.products.productDetail.status === 'INITIATED' ||
                    this.props.bucket.cartData.status === 'INITIATED' ||
                    this.props.cart.submitCartItem.status === 'INITIATED' ||
                    this.props.auth.status === 'INITIATED' ||
                    this.props.products.productList.status === 'INITIATED'||
                    this.props.removeItemData.removeData.status === 'INITIATED') && <Spinner />}
                <div className="row">
                    <div className="col-lg-8">
                        <div className="mb-3">
                            <div className="wish-list">
                                <div className="header-part mb-4">
                                    <div className="mb-2 mt-2">
                                        <span className="a-size-base">
                                            <span className="sc-without-fresh-inline">
                                                Subtotal ({this.props.bucket.cartData.data.length} items):&nbsp;
                            </span>
                                        </span>
                                        <span className="a-color-price a-text-bold">
                                            <span className="a-size-base a-color-price sc-price sc-white-space-nowrap sc-price-sign" style={{color:'#B12704'}}>
                                                <span className="currencyINR" style={{color:'#B12704'}}></span>&nbsp; &#8377; {this.state.totalPrice ? this.numberWithCommas(this.state.totalPrice): 0}
            </span>
                                        </span>
                                    </div>
                                    <div className="a-alert-content" style={{ color: '#067D62' }}>
                                        <span><i className="fa fa-check-circle mr-1" aria-hidden="true" /></span>
                                        <span>
                                            Your order is eligible for FREE Delivery. </span>
                                    </div>
                                </div>
                                {
                                    this.props.bucket.cartData && this.props.bucket.cartData.data.map((item, index) => (
                                        <React.Fragment key={index}>
                                            <div className="row mb-4">
                                                <div className="col-md-5 col-lg-3 col-xl-3">
                                                    <div className="view zoom overlay z-depth-1 rounded mb-3 mb-md-0 cart-item-image-container">
                                                        <img className="img-fluid w-100"
                                                            src={item.imageUrl} alt="Sample" />
                                                        <a href="#!">
                                                            <div className="mask">
                                                                <img className="img-fluid"
                                                                    src={item.imageUrl} />
                                                                <div className="mask rgba-black-slight"></div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div className="d-md-none">
                                                        <div className="cart-item-image-mobile">
                                                            <div className="image">
                                                                <img className=""
                                                                    src={item.imageUrl} alt="Sample" />
                                                            </div>
                                                            <div className="cart-item-name">
                                                                {item.itemName}

                                                                <div>
                                                                    {
                                                                        <div className="">
                                                                            <div className="quantity-container">
                                                                                <span className="minus-button">
                                                                                    <button type="button" className="quantity-left-minus" data-type="minus" data-field="">
                                                                                        <span className=""><i className="fa fa-minus" aria-hidden="true"></i></span>
                                                                                    </button>
                                                                                </span>
                                                                                <input type="text" id="quantity" name="quantity" className=" quantity-input" value={item.quantity} min="1" max="100" />
                                                                                <span className="plus-button">
                                                                                    <button type="button" className="quantity-right-plus" data-type="plus" data-field="">
                                                                                        <span className=""><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                                                    </button>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    }

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-7 col-lg-9 col-xl-9">
                                                    <div style={{height: '100%'}}>
                                                        <div className="d-flex justify-content-between">
                                                            <div className={'hidden-xs'}>
                                                                <h5>{item.itemName}</h5>

                                                            </div>
                                                            <div>
                                                                <div className="desktop-view hidden-xs">
                                                            <div className="quantity-container">
                                                                                <span className="minus-button">
                                                                                    <button type="button" className="quantity-left-minus" data-type="minus" data-field="">
                                                                                        <span className=""><i className="fa fa-minus" aria-hidden="true"></i></span>
                                                                                    </button>
                                                                                </span>
                                                                                <input type="text" id="quantity" name="quantity" className=" quantity-input" value={item.quantity} min="1" max="100" />
                                                                                <span className="plus-button">
                                                                                    <button type="button" className="quantity-right-plus" data-type="plus" data-field="">
                                                                                        <span className=""><i className="fa fa-plus" aria-hidden="true"></i></span>
                                                                                    </button>
                                                                                </span>
                                                                            </div>

                                                                </div>
                                                                <small id="passwordHelpBlock" className="form-text text-muted text-center">

                                                                </small>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex justify-content-between align-items-center " style={{height: '100%'}}>

                                                            <div
                                                                // tslint:disable-next-line:jsx-no-lambda
                                                                onClick={() => this.props.removeItem({
                                                                    product: {
                                                                        itemName: item.itemName,
                                                                        quantity: item.quantity,
                                                                        unitPrice: item.unitPrice,
                                                                        id: item.id
                                                                    }
                                                                })}>
                                                                <span className="card-link-secondary remove-item-button small cursor-pointer text-uppercase mr-3"><i
                                                                    className="fas fa-trash-alt mr-1"></i> Remove item </span>
                                                            </div>
                                                            <p className="mb-0"><span><strong id="summary">&#8377; {item.unitPrice ? this.numberWithCommas(item.unitPrice): 0}</strong></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr className="mb-4" />
                                        </React.Fragment>
                                    ))
                                }
                                <p className="text-primary mb-0"><i className="fas fa-info-circle mr-1"></i> Do not delay the purchase, adding
            items to your cart does not mean booking them.</p>

                            </div>
                        </div>



                        <div className="mb-3">
                            <div className="pt-4">

                                <h5 className="mb-4">Expected shipping delivery</h5>

                                <p className="mb-0"> Thu., 12.03. - Mon., 16.03.</p>
                            </div>
                        </div>



                        <div className="mb-3">
                            <div className="pt-4">

                                <h5 className="mb-4">We accept</h5>

                                <img className="mr-2" width="45px"
                                    src="https://mdbootstrap.com/wp-content/plugins/woocommerce-gateway-stripe/assets/images/visa.svg"
                                    alt="Visa" />
                                <img className="mr-2" width="45px"
                                    src="https://mdbootstrap.com/wp-content/plugins/woocommerce-gateway-stripe/assets/images/amex.svg"
                                    alt="American Express" />
                                <img className="mr-2" width="45px"
                                    src="https://mdbootstrap.com/wp-content/plugins/woocommerce-gateway-stripe/assets/images/mastercard.svg"
                                    alt="Mastercard" />
                                <img className="mr-2" width="45px"
                                    src="https://mdbootstrap.com/wp-content/plugins/woocommerce/includes/gateways/paypal/assets/images/paypal.png"
                                    alt="PayPal acceptance mark" />
                            </div>
                        </div>


                    </div>



                    <div className="col-lg-4">
                        <div className="mb-3">
                            <div className="pt-4">
                                <h5 className="mb-3">The total amount of</h5>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                                        Temporary amount
                                <span>&#8377; {this.state.totalPrice ? this.numberWithCommas(this.state.totalPrice): 0}</span>
                                    </li>
                                    <li className="list-group-item d-flex justify-content-between align-items-center px-0">
                                        Shipping
              <span>Gratis</span>
                                    </li>
                                    <li className="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                                        <div>
                                            <strong>The total amount of</strong>
                                            <strong>
                                                <p className="mb-0">(including GST)</p>
                                            </strong>
                                        </div>
                                        <span><strong>&#8377; {this.state.totalPrice ? this.numberWithCommas(this.state.totalPrice): 0}</strong></span>
                                    </li>
                                </ul>

                                <button type="button" onClick={() => this.props.history.push('/checkout')} className="btn btn-primary btn-block">go to checkout</button>

                            </div>
                        </div>



                        <div className="mb-3">
                            <div className="pt-4">

                                <a className="dark-grey-text d-flex justify-content-between" data-toggle="collapse" href="#collapseExample"
                                    aria-expanded="false" aria-controls="collapseExample">
                                    Add a discount code (optional)
            <span><i className="fas fa-chevron-down pt-1"></i></span>
                                </a>

                                <div className="collapse" id="collapseExample">
                                    <div className="mt-3">
                                        <div className="md-form md-outline mb-0">
                                            <input type="text" id="discount-code" className="form-control font-weight-light"
                                                placeholder="Enter discount code" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
        )
    }
    private numberWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

const mapStateToProps = (state: IOrderSummaryStateProps) => {
    return {
        products: state.products,
        auth: state.auth,
        cart: state.cart,
        bucket: state.bucket,
        removeItemData: state.removeItemData
    }
}

export const mapDispatchToProps = (
    dispatch
): IOrderSummaryProps => {
    return {
        loadUser: () => dispatch(loadUser()),
        loadProductDetail: (productId) => dispatch(loadProductDetail(productId)),
        logout: () => dispatch(logout()),
        loadCart: () => dispatch(loadCart()),
        loadProducts: () => dispatch(loadProducts()),
        removeItem: (itemDetail) => dispatch(removeItem(itemDetail)),
        

    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OrderSummary))