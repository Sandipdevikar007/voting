import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router';



class EmptyCart extends React.Component<RouteComponentProps<{}>,{}>{

    render(){
        return (
            <div className="container-fluid mt-100">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">
                            </div>
                            <div className="card-body cart">
                                <div className="col-sm-12 empty-cart-cls text-center"> <img src="https://i.imgur.com/dCdflKN.png" width="130" height="130" className="img-fluid mb-4 mr-3" />
                                    <h3><strong>Your Cart is Empty</strong></h3>
                                    <h4>Add something to make me happy :)</h4> <button className="btn btn-primary cart-btn-transform m-3" onClick={()=> this.props.history.push('/')} data-abc="true">continue shopping</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(EmptyCart);