
export function scrollToElement(elementRef) {
    const top = elementRef.getBoundingClientRect().top;
    window.scrollTo({
        top: (top + window.pageYOffset) - (window.innerHeight / 2),
        behavior: 'auto'
    });
}

export function scrollElementBelowHeader(htmlElementRef) {
    const Sectiontop = htmlElementRef.getBoundingClientRect().top;
    try {

        if (htmlElementRef) {
                window.scrollTo({
                    top: Sectiontop + window.pageYOffset - 105,
                    behavior: 'auto'
                });
        }
    } catch (e) {
        //
    }
}

export function scrollToViewElement(htmlElementRef) {
    try {
        htmlElementRef.scrollIntoView({
            block: 'nearest',
            behavior: 'smooth',
            inline: 'center'
        });

    } catch (e) {
        //
    }
}
