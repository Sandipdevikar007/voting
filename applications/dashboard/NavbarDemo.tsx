import React, { Component } from 'react';



import Toolbar from './components/toolbar/Toolbar'
import SideDrawer from './components/sidedrawer/SideDrawer'
import BackDrop from './components/backdrop/Backdrop';
import Navbar  from './pages/Navbar';


interface INavbarState {
  sideDrawerOpen: boolean
}
class NavbarDemo extends Component<{}, INavbarState> {
  constructor(props) {
    super(props);
    this.state = {
      sideDrawerOpen: false
    };
  }
  render() {
    let sidedrawer;
    let backdrop;
    if (this.state.sideDrawerOpen) {

      backdrop = <BackDrop click={this.backDropClickhandler} />
    }
    return (
      <>
      <div style={{ height: '0px' }} className={'d-block d-sm-none'}>
        <Toolbar DrawerClickHandler={this.drawerToggleClickHandler} />
        <SideDrawer show={this.state.sideDrawerOpen} />
        {backdrop}
      </div>
      <div className="d-none d-sm-block">
      <Navbar/>
      </div>
    </>
    );
  }
  private drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return { sideDrawerOpen: !prevState.sideDrawerOpen }
    });
  }
  private backDropClickhandler = () => {
    this.setState({ sideDrawerOpen: false });
  }
  

}
export default NavbarDemo
