import React from 'react';
import './style.less';
import {Button} from '../../../../components/buttons';
interface IProductList{
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    discount: number
    platfrom: string
}
interface IProductItemProps{
    onClick?: () => void;
    updateClick?: () => void;
    product: IProductList
    selected: boolean;
    isAdmin: boolean;
}
export default class ProductItem extends React.Component<IProductItemProps, {}>{
    public render() {
        return (
            <div className="col-md-3 product-col" >
            <div className={`book-detail-block ${this.props.selected ? 'selected' : ''}`}>
                <div className="book-info">
                    <div className="book-image zoom overlay  rounded">
                    <div className="view hm-zoom cursor-pointer" onClick={this.props.onClick}>
                            <img src={this.props.product.imageUrl} className="img-fluid " alt="" />
                            <div className="mask flex-center">
                                <p className="white-text">View</p>
                            </div>
                        </div>

                    </div>
                    <div className="details">
                    <div className="book-name heading16">
                        {this.props.product.name}
                </div>
                    <div className="margin-top-auto" />
                            <div className="book-price heading16">
                                {
                                    this.props.product.discount && <>
                                        <del className="body16">&#8377;{this.props.product.fromPrice ? this.numberWithCommas(Number(this.props.product.fromPrice)) : 0}</del>
                                    &nbsp;</>
                                }
                     &#8377;{
                                    this.props.product.discount ?
                                        (this.props.product.fromPrice ? this.numberWithCommas(Number(this.props.product.fromPrice) - (Number(this.props.product.discount / 100) * this.props.product.fromPrice)) : 0) : this.props.product.fromPrice ? this.numberWithCommas(Number(this.props.product.fromPrice)) : 0}
                            </div>
                <div className="discount">
                    { this.props.product.discount &&
                        this.props.product.discount + "% off"
                    }
                            </div>
                           <div className="button-container">
                           <div className="select-button-container">
                                <Button label="Select" onClick={this.props.onClick} />
                            </div>
                            {this.props.isAdmin &&
                                <div className="select-button-container">
                                    <Button label="Update" className="update-button" onClick={this.props.updateClick} />
                                </div>
                            }
                            </div>   
                        </div>
                    </div>
            </div>
        </div>
        )
    }
    private numberWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}