import React from 'react';
import './toolbar.less';
import DrawerTogglerButton from '../sidedrawer/DrawerTogglerButton';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../../../actions/auth';
import Navbar from '../../pages/Navbar';
const Toolbar = (props) => {

  const authLinks =
    (
      <>
        <ul>
          <li><Link to="/polls">Polls</Link></li>

          <li><Link to="/result">Result</Link></li>
          <li><Link to="#" onClick={logout}>Logout</Link></li>
        </ul>
      </>
    );

  const adminLinks = (

    <>
      <ul >
        <li ><Link to="/addPolls">Add Polls</Link></li>

        <li ><Link to="/addResult">Add Result</Link></li>
        <li ><Link to="/polls" >Polls</Link></li>
        <li ><Link to="/pollslist"> Polls List</Link></li>
        <li ><Link to="/result" >Result</Link></li>
        <li ><Link to="#" onClick={logout}>Logout</Link></li>

      </ul>
    </>


  );
  const guestLink = (
    <ul>
      <li><Link to="/polls">Polls</Link></li>
      <li><Link to="/register">Register</Link></li>
      <li><Link to="/login">Login</Link></li>
    </ul>

  );
  const  highlightMatch = (label: string, itemId) => {
    let match = label.toLowerCase().match(props.searchText.toLowerCase());
    console.log(match);
    if (match != null && match.index !== undefined) {
        const start = label.substring(0, match.index);
        const matchedText = label.substring(match.index, match.index + props.searchText.length);
        console.log(matchedText);
        const end = label.substring(match.index + props.searchText.length);
        return (
          <span id={`${itemId}-text`} data-item={label}>
            {matchedText}
            <span id={`${itemId}-highlight`} className="tl-dropdown-match" data-item={label}>
              {end}
            </span>

            </span>
        )
    } else {
        return <span id={`${itemId}-text`} data-item={label}>{label}</span>
    }
  }
  return (
    <header className="toolbar">
      <nav className="toolbar-navigation">
        <div className={'navbar-toggler'}><DrawerTogglerButton click={props.DrawerClickHandler} /></div>
        <a className="navbar-author" href="#!">
          <img src="https://mdbootstrap.com/img/logo/mdb-transaprent-noshadows.png" height="30" alt="mdb logo" />
        </a>
        <span className="nav-link navbar-link-2 waves-effect d-block d-sm-none ml-auto" onClick={props.shopCartClick}>
          <span className="badge badge-pill red">{props.bucket.cartData.data &&
            props.bucket.cartData.data.length !== 0 && props.bucket.cartData.data.length}</span>
          <i className="fas fa-shopping-cart pl-0" />
        </span>
      </nav>
      <div className="nav-search-container visible-xs col-md-6">
        <form className="input-group md-form form-sm form-2 pl-0" onSubmit={props.onSearchProduct}>

          <input className="form-control my-0 py-1 input-searh red-border" onKeyDown={props.onKeyPress} value={props.searchText} onChange={props.onSearchTextChange} type="text" autoComplete={''} placeholder="Search" aria-label="Search" />
         {
           props.searchText &&
           <div className="input-group-append">
           <span className="input-group-text cursor-pointer search-clear" onClick={props.clearSearch} id="basic-text1"><i className="fas fa-times text-grey"
             aria-hidden="true"></i></span>
         </div>
         }
          <div className="input-group-append">
            <span className="input-group-text cursor-pointer" id="basic-text1" onClick={props.onSearchProduct}><i className="fas fa-search text-grey"
              aria-hidden="true"></i></span>
          </div>
          { props.isOpen&&
                            props.searchSuggestions &&
                            props.searchSuggestions.length > 0 &&
                                (
              <div className="suggestions-container" id={'mobile-suggestions-container'}>
                {props.searchSuggestions.map((item, index) => (
                  <div key={index}>
                    <div className={'cursor-pointer suggestion-item'} onClick={() => props.onSearchTextChange({
                      target: {
                        value: item,
                        suggestionClick: true
                      }
                    })}>{highlightMatch(item, index)}</div>
                  </div>
                ))}

                                    </div>
                                )
                            }
        </form>
      </div>
    </header>
  );
  
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    auth: state.auth,
    loading: state.auth.loading,
    cart: state.cart,
    bucket: state.bucket
  }
}

export default connect(mapStateToProps, { logout })(Toolbar);