import React from 'react';
import './style.less';
import Resizer from 'react-image-file-resizer';
import axios from 'axios';
import Spinner from '../../../../applications/layout/Spinner';
import { v4 as uuidv4 } from 'uuid';
import {submitProductItem} from '../../../actions/product-upload';
import { connect } from 'react-redux';
import productUpload from 'applications/reducers/product-upload';
import { withRouter, RouteComponentProps } from 'react-router';
interface IProductList{
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    platfrom: string
}interface ILoginData {
    email: string;
    password: string
}
interface ISelectedItem {
    product: {
        itemName: string,
        quantity: number,
        unitPrice: number,
        id: string,
        imageUrl: string,
        discount?: number
    }
}
interface IProducts {
    productDetail: IProductDetail
    productList: IProducts,
    search: {
        searchText: string,
        searchFlag: boolean
    }
}
interface IProductDetail {
    data: IProductList,
    status: string
}
interface IProducts {
    data: IProductList[],
    status: string
}
interface IProductList {
    name: string,
    sku: string,
    productId: string,
    imageUrl: string,
    fromPrice: number,
    author: string,
    platfrom: string,
    _id: string,
    additionalDescription: string,
    description: string,
    additionalImages: string[],
    features: string,
    rating: string,
    categories: string[],
    discount: number
}

interface ISubmitCart{
    submitCartItem: {
        data: object,
        status: string
    }
}
interface IBucket{
    cartData: {
        data: object[],
        status: string
    }
}
interface IUploadPagetateProps {
    productUpload: any;
    products: IProducts,
    auth: IAuth,
    cart: ISubmitCart,
    bucket: IBucket,
    alert: {
        msg: string,
        alertType: string
    }[]
}
interface IAuth {
    token: string,
    isAuthenticated: boolean,
    loading: boolean
    user: any,
    status: string
}
interface IToastArr{
    id: string,
    description: string,
    type: string,
    time: number
}
interface IProductItemProps{
    onClick?: () => void;
    product: IProductList
    selected: boolean;
    submitProductItem: (data) => void;
    productUpload: IProductUpload
}
interface IProductUpload{
    productUploadData: {
        data: object,
        status: string
    }
}
interface IProductUploadState {
    totalPrice: 0,
    itemName: string,
    author: string,
    unitPrice: string | 0,
    discount: string | 0,
    description: string,
    additionalDescription: string,
    additionalImages: string[],
    rating: string,
    features: string,
    city: string,
    fileUploadStatus: string
    imageUrl: string,
    categories: string[],
    categoryInput: string
}
export type IProps = IProductItemProps & IUploadPagetateProps & RouteComponentProps<{type: string}>
export class ProductUpload extends React.Component<IProps, IProductUploadState>{

    constructor(props) {
        super(props);
        this.state = {
            totalPrice: 0,
            itemName: '',
            author: '',
            unitPrice: '',
            discount: '',
            description: '',
            additionalDescription: '',
            additionalImages: [],
            rating: '',
            features: '',
            city: '',
            imageUrl: '',
            fileUploadStatus: 'NOT STARTED',
            categories: [],
            categoryInput: ''
        }
    }
    public componentWillReceiveProps(newProps: IProps) {
        if (newProps.productUpload.productUploadData.status !== this.props.productUpload.productUploadData.status){
            if (newProps.productUpload.productUploadData.status === 'SUCCESS') {
                this.setState({
                    totalPrice: 0,
                    itemName: '',
                    author: '',
                    unitPrice: '',
                    discount: '',
                    description: '',
                    additionalDescription: '',
                    additionalImages: [],
                    rating: '',
                    features: '',
                    city: '',
                    imageUrl: '',
                })
            }
        }
    }

    public render() {
        return (
            <div className="">
                <main className="">
                {(this.props.products.productDetail.status === 'INITIATED' ||
                    this.props.products.productList.status === 'INITIATED' ||
                    this.props.cart.submitCartItem.status === 'INITIATED' ||
                    this.props.productUpload.productUploadData.status === 'INITIATED' ||
                    this.props.auth.status === 'INITIATED') && <Spinner />}
  <div className="container wow fadeIn">

                        {this.props.productUpload.productUploadData.status === 'INITIATED' || this.state.fileUploadStatus === 'INITIATED' && <Spinner />}
                        <h2 className="mb-5 mt-5 ml-5 h2 text-center">Product upload</h2>


                        <div className="row">
                            <div className="col-md-2"/>

                            <div className="col-md-8 mb-4">


        <div className="card">


          <form className="card-body">


            <div className="row">


              <div className="col-md-6 mb-2">


                <div className="md-form ">
                  <input type="text" id="itemName" value={this.state.itemName} onChange={(e) => this.setState({itemName: e.target.value})} className="form-control" required />
                  <label htmlFor="itemName" className="">Book Name</label>
                </div>

              </div>



              <div className="col-md-6 mb-2">


                <div className="md-form">
                  <input type="text" id="unitPrice" value={this.state.unitPrice} onChange={(e) => this.setState({unitPrice: e.target.value})} className="form-control" required />
                  <label htmlFor="unitPrice" className="">Unit Price</label>
                </div>

              </div>


            </div>



            <div className="md-form input-group pl-0 mb-5">
              <div className="input-group-prepend">
                <span className="input-group-text" id="basic-addon1">@</span>
              </div>
              <input type="text" className="form-control py-0" value={this.state.author} onChange={(e) => this.setState({author: e.target.value})} placeholder="Author Name" aria-describedby="basic-addon1" required/>
            </div>
            <div className="md-form mb-5">
              <input type="text" id="description" value={this.state.discount} onChange={(e) => this.setState({discount: e.target.value})}  className="form-control" />
              <label htmlFor="description" className="">Give Discount (%)</label>
            </div>
            <div className="md-form mb-5">
              <input type="text" id="description" value={this.state.description} onChange={(e) => this.setState({description: e.target.value})}  className="form-control" />
              <label htmlFor="description" className="">Description</label>
            </div>
            <div className="md-form mb-5">
              <input type="text" id="features" className="form-control" required value={this.state.features} onChange={(e) => this.setState({features: e.target.value})}/>
              <label htmlFor="features" className="">Features</label>
            </div>
            <div className="md-form mb-5">
              <input type="text" id="address-2" value={this.state.additionalDescription} onChange={(e) => this.setState({additionalDescription: e.target.value})}  className="form-control"/>
              <label htmlFor="address-2" className="">Additional description</label>
            </div>
            <div className="md-form mb-5">
              <input type="text" id="category" value={this.state.categoryInput} onChange={(e) => this.setState({categoryInput: e.target.value})}  className="form-control"/>
              <label htmlFor="category" className="">Add categories (separated with comma)</label>
            </div>
            <div className=" d-none md-form mb-5">
              <input type="text" id="city" value={this.state.city} onChange={(e) => this.setState({city: e.target.value})}  className="form-control"/>
              <label htmlFor="city" className="">City</label>
            </div>

            <div className="row">


              <div className="col-lg-4 col-md-12 mb-4">

                <label htmlFor="rating">Rating</label>
                <select className="custom-select d-block w-100" value={this.state.rating} onChange={(e) => this.setState({rating: e.target.value})} id="rating">
                  <option value="">Choose...</option>
                  <option>5</option>
                  <option>4</option>
                  <option>3</option>
                  <option>3</option>
                  <option>1</option>
                </select>
                <div className="invalid-feedback">
                  Please select a valid rating.
                  </div>

              </div>



              <div className=" d-none col-lg-4 col-md-6 mb-4">

                <label htmlFor="state">State</label>
                <select className="custom-select d-block w-100" value={this.state.rating} onChange={(e) => this.setState({rating: e.target.value})}  id="state">
                  <option value="">Choose...</option>
                  <option>Maharashtra</option>
                </select>
                <div className="invalid-feedback">
                  Please provide a valid state.
                  </div>

              </div>
                                            <div className="row ">
                                                <div className="col-3"/>
                                                <div className="  col-6 file-field">
                                                    <div className="btn btn-sm float-left">
                                                        <input type="file"
                                                            multiple

                                                            accept="images/*"
                                                            onChange={this.fileUploadHandler} />
                                                    </div>
                                                </div>

                                            </div>

            </div>


            <hr />

            <div className="d-none my-3">
              <div className="custom-control custom-radio">
                <input id="credit" name="paymentMethod" type="radio" className="custom-control-input" checked required />
                <label className="custom-control-label" htmlFor="credit">Credit card</label>
              </div>
              <div className="custom-control custom-radio">
                <input id="debit" name="paymentMethod" type="radio" className="custom-control-input" required />
                <label className="custom-control-label" htmlFor="debit">Debit card</label>
              </div>
              <div className="custom-control custom-radio">
                <input id="paypal" name="paymentMethod" type="radio" className="custom-control-input" required />
                <label className="custom-control-label" htmlFor="paypal">Paypal</label>
              </div>
            </div>
            <div className="row d-none">
              <div className="col-md-6 mb-3">
                <label htmlFor="cc-name">Name on card</label>
                <input type="text" className="form-control" id="cc-name" placeholder="" required />
                <small className="text-muted">Full name as displayed on card</small>
                <div className="invalid-feedback">
                  Name on card is required
                  </div>
              </div>
              <div className="col-md-6 mb-3">
                <label htmlFor="cc-number">Credit card number</label>
                <input type="text" className="form-control" id="cc-number" placeholder="" required />
                <div className="invalid-feedback">
                  Credit card number is required
                  </div>
              </div>
            </div>
            <div className="row d-none">
              <div className="col-md-3 mb-3">
                <label htmlFor="cc-expiration">Expiration</label>
                <input type="text" className="form-control" id="cc-expiration" placeholder="" required />
                <div className="invalid-feedback">
                  Expiration date required
                  </div>
              </div>
              <div className="col-md-3 mb-3">
                <label htmlFor="cc-expiration">CVV</label>
                <input type="text" className="form-control" id="cc-cvv" placeholder="" required />
                <div className="invalid-feedback">
                  Security code required
                  </div>
              </div>
            </div>
            <hr className="mb-4" />
            <button className="btn btn-primary btn-lg btn-block" onClick={this.submitProduct} type="submit">Submit Order</button>

          </form>

        </div>


      </div>
    </div>


  </div>
</main>
            </div>
        )
    }

    private submitProduct = () => {
        const id = uuidv4();
        console.log(this.state.categoryInput)
        const productDetail =
        {
            name: this.state.itemName,
            productId: id ,
            author: this.state.author,
            platform: '',
            imageUrl: this.state.imageUrl,
            fromPrice: this.state.unitPrice,
            sku: '',
            additionalDescription: this.state.additionalDescription,
            description: this.state.description,
            additionalImages: this.state.additionalImages,
            features: this.state.features,
            rating: this.state.rating,
            discount: this.state.discount,
            categories: this.state.categoryInput.split(',')

        }
        if (this.state.itemName && this.state.imageUrl && this.state.unitPrice) {
            this.props.submitProductItem(productDetail)
        }

    }
    private fileUploadHandler = (e) => {
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    };
    this.setState({
      fileUploadStatus: 'INITIATED'
    })
    for (let i = 0; i < e.target.files.length; i++) {
      Resizer.imageFileResizer(e.target.files[i], 300, 300, 'JPEG', 100, 0,
        async uri => {
          await axios.post('api/image/upload', { image: uri }, config).then((res) => {
            if (i === 0) {
              this.setState({
                imageUrl: res.data.url,
                fileUploadStatus: 'SUCCESS'
              })
            } else {
              const additionalImages = this.state.additionalImages
              additionalImages.push(res.data.url)
              this.setState({
                additionalImages,
                fileUploadStatus: 'SUCCESS'
              })
            }
          }).catch((err) => {
            console.log(err)
          });
        },
        'base64'
      );
    }
  }
}

const mapStateToProps = (state : IUploadPagetateProps)  => {
    return {
        products: state.products,
        auth: state.auth,
        cart: state.cart,
        bucket: state.bucket,
        productUpload: state.productUpload
    }
}
export default withRouter(connect(mapStateToProps, {submitProductItem})(ProductUpload))
