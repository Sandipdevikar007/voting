import React from 'react';
import './SideDrawer.less';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../../../actions/auth';
const SideDrawer = (props) => {
    let drawerClasses = 'side-drawer';

    if (props.show) {
        drawerClasses = 'side-drawer open'
    }



    return (

        <nav className={drawerClasses}>
            <div className="cross-icon" onClick={props.backDropClickhandler}>
                <img src="https://res.cloudinary.com/daxgp6jsz/image/upload/v1605963966/nakxfmdzi0xudvufcs0l.png" alt={''} />
            </div>
            <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                    <span className="nav-link navbar-link-2 waves-effect d-none d-sm-block " onClick={props.shopCartClick}>
                        <span className="badge badge-pill red">{props.bucket.cartData.data &&
                            props.bucket.cartData.data.length !== 0 && props.bucket.cartData.data.length}</span>
                        <i className="fas fa-shopping-cart pl-0" />
                    </span>
                </li>
                {
                    props.auth.user && props.auth.user.UserType &&
                    props.auth.user.UserType.toLocaleLowerCase() === 'admin' &&
                    <li className="nav-item">
                        <span data-toggle="collapse" data-target="#basicExampleNav1" onClick={props.ordersClick} className="nav-link waves-effect">
                            Orders
</span>
                    </li>
                }
                {
                    props.auth.user && props.auth.user.UserType &&
                    props.auth.user.UserType.toLocaleLowerCase() === 'admin' &&
                    <li className="nav-item">
                        <span data-toggle="collapse" data-target="#basicExampleNav1" onClick={props.addBookClick} className="nav-link waves-effect">
                            Add Book
</span>
                    </li>
                }
                <li className="nav-item" onClick={props.onHomePageGo}>
                    <span className="nav-link waves-effect" >
                        Shop
</span>
                </li>
                <li className="nav-item">
                    <span className="nav-link waves-effect">
                        Contact
</span>
                </li>
                {!props.auth.isAuthenticated && <li className="nav-item">
                    <span className="nav-link waves-effect" data-toggle="modal" onClick={props.backDropClickhandler} data-target="#elegantModalForm">
                        Sign in
</span>
                </li>}
                {props.auth.isAuthenticated && <li className="nav-item">
                    <span className="nav-link waves-effect" onClick={() => {
                        props.backDropClickhandler()
                        props.logout()
                    }}>
                        Log out
</span>
                </li>}
                {!props.auth.isAuthenticated && (
                    <li className="nav-item pl-2 mb-2 mb-md-0">
                        <button type="button"
                            onClick={props.backDropClickhandler}
                            className="btn btn-outline-info btn-md btn-rounded btn-navbar waves-effect waves-light"
                            data-toggle="modal" data-target="#elegantModalSignupForm">Sign up</button>
                    </li>)
                }
            </ul>
        </nav>


    )

}
const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        auth: state.auth,
        loading: state.auth.loading,
        products: state.products,

        cart: state.cart,
        bucket: state.bucket
    }
}

export default connect(mapStateToProps, { logout })(SideDrawer);