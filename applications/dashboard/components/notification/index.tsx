import React from 'react';
import './toast.less'
interface IToastProps {
    className?: string;
    id?: string;
    toastList?: IToastListProps[];
    position?: string;
    autoDelete?: boolean;
    autoDeleteTime?: number;
}

interface IToastListProps {
    id?: string;
    description?: string;
    type: string;
    toastIcon?: boolean;
    closeIcon?: boolean;
}

interface IToastState {
    bannerSizeClass?: string;
    toastList: IToastListProps[],
    autoDelete: boolean,
    autoDeleteTime: number,
    position: string,
    smallDeviceClass: string
}

const smallScreen = 1024;

export default class Toast extends React.Component<IToastProps, IToastState> {
    constructor(props) {
        super(props);
        this.state = {
            toastList: [],
            autoDelete: false,
            autoDeleteTime: 0,
            position: '',
            smallDeviceClass: ''
        }
        this.updateDimensions = this.updateDimensions.bind(this)
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.toastList !== prevState.toastList) {
            return {
                toastList: nextProps.toastList,
                autoDelete: nextProps.autoDelete,
                autoDeleteTime: nextProps.autoDeleteTime,
                position: nextProps.position
            };
        } else
            return null;
    }

    selfDelete = () => {
        let { autoDelete, autoDeleteTime, toastList } = this.state;
        let selfDeleteIntervalClock = autoDeleteTime ? autoDeleteTime + 5000 : 10000;

        const interval = setInterval(() => {
            var time = Date.now();
            if (autoDelete && toastList.length) {
                let first = [...toastList];
                let extendedTime = first[0]['time'] + autoDeleteTime;
                if (time > extendedTime) {
                    this.deleteToast(first[0]['id']);
                }
            }
        }, 500);

        setTimeout(() => {
            clearInterval(interval);
        }, selfDeleteIntervalClock)
    }


    deleteToast = id => {
        let { toastList } = this.state;
        if (toastList.length) {
            const listItemIndex = toastList.findIndex(e => e['id'] === id);
            toastList.splice(listItemIndex, 1);
            this.setState({ toastList })
        }
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
    }

    updateDimensions() {
        let modalClass = '';
        let windowInnerWidth = window.innerWidth;

        if (windowInnerWidth < smallScreen) {
            modalClass = 'mobile-device';
        }
        this.setState({ smallDeviceClass: modalClass });
    }

    getToastType = (type) => {
        let toastClassName = 'mb-toast-generic';
        let toastIconName = 'icon-system-info-small';
        if (type === 'generic') {
            toastClassName = 'mb-toast-generic';
            toastIconName = 'icon-system-info-small';
        } else if (type === 'success') {
            toastClassName = 'mb-toast-success';
            toastIconName = 'icon-system-tick-copy';
        } else if (type === 'warning') {
            toastClassName = 'mb-toast-warning';
            toastIconName = 'icon-system-alert-small';
        }
        toastClassName += ' ' + this.state.smallDeviceClass;
        return { toastClassName, toastIconName };
    }

    getToastMessageClass = (message) => {
        return message.length > 42 ? 'mb-toast-long-message' : '';
    }

    getToastMessage = (message) => {
        let msgLengthValue = 80;
        if (this.state.smallDeviceClass) {
            msgLengthValue = 52;
        }
        let messageHeight = message.length > msgLengthValue ? message.substring(0, msgLengthValue) + '...' : message;
        return messageHeight;
    }

    render() {
        let { toastList, position } = this.state;
        if (toastList.length) {
            this.selfDelete();
        }
        let toastContainerClass = "mb-toast-container mb-toast-" + position + ' ' + this.state.smallDeviceClass;
        return (
            <div className={toastContainerClass}>
                {
                    toastList.map((toast, i) =>
                        <div
                            key={i}
                            className={`mb-toast  ${position} ${this.getToastType(toast.type).toastClassName}`}
                            role={toast.type === 'warning' ? "alert" : 'status'} aria-live={toast.type === 'warning' ? "assertive" : 'polite'} aria-atomic="true"
                        >
                            <div aria-label="Close" className={`mb-toast-cross-button ${toast.closeIcon === false ? "mb-toast-hide" : 'mb-toast-show'}`} onClick={() => this.deleteToast(toast.id)}>
                                <span className="mb-toast-cross-icon"><i className="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                            <div className={`mb-toast-image ${toast.toastIcon === false ? "mb-toast-hide" : 'mb-toast-show'}`}>
                                <span className={`mb-toast-left-icon ${toast.type === 'success'? 'success': 'error'}`}>{toast.type === 'success' ? <i className="fa fa-check" aria-hidden="true"></i> :
                                    <i className="fa fa-info-circle" aria-hidden="true"></i>}</span>
                            </div>
                            <div>
                                <p className={`mb-toast-message ${this.getToastMessageClass(toast.description)}`}>
                                    {this.getToastMessage(toast.description)}
                                </p>

                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}
