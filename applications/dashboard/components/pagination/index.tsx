import React from "react";
import { Button } from '../../../../components/buttons'
const maxWidth = 768; // above medium device
const minWidth = 320; // small device
import './style.less'

const defaultTablePageSizeOptions = ['25', '50', '100'];
let oldPage = 0;

export default class Pagination extends React.Component<any, any> {
  constructor(props) {
    super(props);

    this.getSafePage = this.getSafePage.bind(this);
    this.changePage = this.changePage.bind(this);
    this.applyPage = this.applyPage.bind(this);

    this.state = {
      page: props.page,
      pageUpdated: false,
      minWidth: 0,
    };
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions, false);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions, false);
  }

  updateDimensions() {
    if (window.innerWidth < maxWidth) {
      this.setState({ width: minWidth });
    } else {
      this.setState({ width: window.innerWidth });
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.page !== state.page) {
      return {
        page: props.page
      };
    }
    return null;
  }

  getSafePage(page) {
    if (Number.isNaN(page)) {
      page = this.props.page;
    }
    return Math.min(Math.max(page, 0), this.props.pages - 1);
  }

  changePage(page) {

    console.log(page)
    page = this.getSafePage(page);
    this.setState({ page, pageUpdated: false });
    if (this.props.page !== page) {
      this.props.onPageChange(page);
    }
  }

  applyPage(e) {
    if (e) {
      e.preventDefault();
    }
    const page = this.state.page;
    this.changePage(page === "" ? this.props.page : page);
  }

  filterPages = (visiblePages, totalPages) => {
    return visiblePages.filter((page) => page <= totalPages);
  };

  getVisiblePages = (page, total) => {
    total = parseInt(total);
    if (total < 7) {
      return this.filterPages([1, 2, 3, 4, 5, 6], total);
    } else {
      if (page % 5 >= 0 && page > 4 && page + 2 < total) {
        return [1, page - 1, page, page + 1, total];
      } else if (page % 5 >= 0 && page > 4 && page + 2 >= total) {
        return [1, total - 3, total - 2, total - 1, total];
      } else {
        return [1, 2, 3, 4, 5, total];
      }
    }
  };

  onPageSizeValChange = (page) => {
    setTimeout(() => {
      this.changePage(0);
    }, 50);
    this.props.onPageSizeChange(page);
  };

  updatePage = (page) => {
    this.setState({page: page, pageUpdated: true})
  }

  render() {
    const { pages, pageSize, canPrevious, totalRecordLength, restorePageNumber, pageSizeOptions } = this.props;
    let defaultTablePageSize = pageSizeOptions ? pageSizeOptions : defaultTablePageSizeOptions;

    let { page, canNext } = this.props;
    if(restorePageNumber && !this.state.pageUpdated){
      if(page === oldPage){
        oldPage = page;
        page = 0;
      }
    }
    oldPage = page;
    canNext = canNext === false && page === 0 ? true : canNext;
    const visiblePages = this.getVisiblePages(page + 1, pages);
    const activePage = page + 1;
    const firstRecord = page * pageSize;
    let lastRecord = firstRecord + pageSize;
    let totalRecords = totalRecordLength;
    lastRecord = lastRecord >= totalRecordLength ? totalRecordLength : lastRecord;
    const dropdownOptions: any = [
      { displayText: defaultTablePageSize[0], itemId: defaultTablePageSize[0], selectable: true },
      { displayText: defaultTablePageSize[1], itemId: defaultTablePageSize[1], selectable: false },
      { displayText: defaultTablePageSize[2], itemId: defaultTablePageSize[2], selectable: false },
    ];
    const showDropdown = totalRecords > defaultTablePageSize[0] ? true : false;
    const showPages = pages > 1 ? true : false;

    return (
      <div className="pagination-wrapper" id="lazy">
        <div className="page-info">
          <span className="pagination-text-count">
            {`${firstRecord + 1}-${lastRecord}`}{" "}
          </span>
          <span className="pagination-text-light">{"of"} </span>
          <span className="pagination-text-bold">{`${totalRecords}`} </span>
          <span className="pagination-text-light pagination-records">
            {"Items per page"}
          </span>

          { showDropdown && <div className="pagination-dropdown">
                {this.props.pageSize}
          </div> }
        </div>
        { showPages && <div className="pg-button-wrapper text">
          <Button
            onClick={() => {
              if (!canPrevious) return;
              this.changePage(page - 1);
            }}
            variant="primary"
            disabled={!canPrevious}
            className={!canPrevious?"disabled-button":""}
          >
            {this.state.width > minWidth ? "Previous" : "<-"}
          </Button>
          {visiblePages.map((page, index, array) => {
            return (
              <React.Fragment key={page}>
                {array[index - 1] + 2 <= page ? (
                  <div className="pg-range-dot">...</div>
                ) : (
                  ""
                )}
                <Button
                  onClick={this.changePage.bind(null, page - 1)}
                  variant="secondary"
                  className={
                    "pagination-item" +
                    (activePage === page ? " pagination-selected" : "")
                  }
                  disabled={activePage === page ? true : false}
                >
                  {page}
                </Button>
              </React.Fragment>
            );
          })}
          <Button
            onClick={() => {
              if (!canNext) return;
              this.changePage(page + 1);
            }}
            className={!canNext?"disabled-button":""}
            disabled={!canNext}
            variant="primary"
          >
            {this.state.width > minWidth ? "Next" : "->"}
          </Button>
          </div> }
      </div>

    );
  }
}
