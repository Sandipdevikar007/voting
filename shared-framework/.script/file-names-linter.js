const fs = require('fs'); const path = require('path');
const globPath = path.join(process.cwd(), 'node_modules', 'glob'); const configPath = path.join(process.cwd(), 'package.json');
const glob = require(globPath); const config = require(configPath);
function kebabizeFilename(filename) { return filename.replace(/[A-Z][a-z]*/g, (item) => '-' + item.toLowerCase()).replace(/^-/, '').replace(/\/-/g, '\/'); }
function verifyFileNaming(validNameRegex, globs, nameFixer, fixNames) {
    let areAllFilesValid = true;
    const targetFiles = globs.reduce((files, pattern) => files.concat(glob.sync(pattern)), []); targetFiles.forEach((file) => {
        if (!validNameRegex.test(file)) {
            const fixedName = nameFixer(file); areAllFilesValid = false;
            if (fixNames) { console.log('File ', file, ' has been renamed to ', fixedName); fs.rename(file, fixedName); } else { console.error('File ', file, ' should be named ', fixedName); }
        }
    });
    return areAllFilesValid;
}
function getProperTestFileName(file) { const fileNameRegex = /(\/[\-\w]+?)(-tests)?\.(tsx?)$/; return file.replace(fileNameRegex, (match, fileName, testsPostfix, extension) => `${kebabizeFilename(fileName)}.test.${extension}`); }
function verifyTestFiles(fixNames = false) {
    const validTestFileName = /(\/[a-z\-]*)((\.test)|(\.d))\.tsx?$/;
    return verifyFileNaming(validTestFileName, config.filesNames.testFilesGlobs, getProperTestFileName, fixNames);
}
function getProperScriptFileName(file) { const fileNameRegex = /(\/[\-\w]+?)\.(tsx?|less)$/; return file.replace(fileNameRegex, (match, fileName, extension) => `${kebabizeFilename(fileName)}.${extension}`); }
function verifyScriptFiles(fixNames = false) {
    const validScriptFileName = /(\/[a-z\-]*)(\.style)?\.(tsx?|less|d\.ts)$/;
    return verifyFileNaming(validScriptFileName, config.filesNames.scriptFilesGlobs, getProperScriptFileName, fixNames);
}
function getProperFolderName(folder) { return kebabizeFilename(folder); }
function verifyFolders(fixNames = false) {
    const validFoldersNaming = /^(\.\.\/)*(__)?[a-z\-]*(__)?(\/(__)?[a-z\-]*(__)?)+$/;
    return verifyFileNaming(validFoldersNaming, config.filesNames.foldersGlobs, getProperFolderName, fixNames);
}
const args = process.argv.slice(2); const shouldFixNames = args.find((arg) => arg === '--fix') !== undefined;
let areAllFilesValid = verifyTestFiles(shouldFixNames); areAllFilesValid = areAllFilesValid & verifyScriptFiles(shouldFixNames); areAllFilesValid = areAllFilesValid & verifyFolders(shouldFixNames); if (!areAllFilesValid) { console.log('--------'); if (shouldFixNames) { console.log('Files names have been fixed successfully.'); } else { console.log('Run \'npm run lint-files-names -- --fix\' to fix files names.'); } console.log('--------'); process.exit(1); }